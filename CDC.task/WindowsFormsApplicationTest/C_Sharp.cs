﻿using System;
using System.Collections.Generic;

namespace TestProject
{
	/*
	 * Задача: реализовать метод CalcRoutes в классе Calculator.
	 * Метод должен распределять документы по машинам с учётом
	 * максимальной грузоподъёмности.
	 * В маршрут необходимо добавлять точки, наиболее близкие к 
	 * последней точке маршрута (по прямой).
	 * Исходная точка - StartLat и StartLon автомобиля.
	 * Допускается остаток неразвозимых документов (все машины заполены).
	 * Порядок выбора автомобилей не важен.
	 * Изменять интерфейсы не допускается.
	 * Изменения в классе Calculator не ограничены (кроме сигнатуры метода CalcRoutes).
	 * Реализация интерфейсов с загрузкой из любых источников приветствуется.
	 * Реализация GUI приветствуется. 
	 * Допускается использование стандартных компонентов и компонентов DevExpress.
	 */

	/// <summary>
	/// Расчётчик маршрутов
	/// </summary>
	public class Calculator
	{
		/// <summary>
		/// Рассчитать маршруты
		/// </summary>
		/// <param name="docs">Список документов</param>
		/// <param name="cars">Список доступных машин</param>
		/// <returns>Список сформированных маршрутов</returns>
		public List<IRoute> CalcRoutes(List<IDoc> docs, List<ICar> cars)
		{
            if (docs == null || cars == null || docs.Count == 0 || cars.Count == 0)
                throw new ArgumentException();

            List<IDoc> tdocs = new List<IDoc>();
            tdocs.AddRange(docs);

            for (int i = 0; i < cars.Count; i++)
            {
                bool overweight = false;
                IRoute r = new Route();
                Car c = (cars[i] as Car);
                while (tdocs.Count > 0 && !overweight)
                {
                    // определение блажайщего груза для обрабатываемой машины
                    IDoc d = tdocs[0];
                    double minDistance = Math.Sqrt((c.CurrentLat - d.Lat) * (c.CurrentLat - d.Lat) +
                                                    (c.CurrentLon - d.Lon) * (c.CurrentLon - d.Lon));
                    for (int j = 1; j < tdocs.Count; j++)
                    {
                        double distance = Math.Sqrt((tdocs[j].Lat - c.CurrentLat) * (tdocs[j].Lat - c.CurrentLat) +
                            (tdocs[j].Lon - c.CurrentLon) * (tdocs[j].Lon - c.CurrentLon));
                        if (distance < minDistance)
                        {
                            d = tdocs[j];
                            minDistance = distance;
                        }
                    }
                    // возможно ли еще добавить ближайших груз
                    if (d.Weight + c.CurrentWeight <= c.MaxWeight)
                    {
                        c.CurrentWeight += d.Weight;
                        c.CurrentLat = d.Lat;
                        c.CurrentLon = d.Lon;
                        r.Docs.Add(d);
                        tdocs.Remove(d);
                    }
                    else
                        overweight = true;

                }
                c.Route = r;
            }

            // для оставшихся грузов выполняем поиск ближайший машины
            // с достаточным количеством грузоподъемности
            for (int i = 0; i < tdocs.Count; i++)
            {
                IDoc d = tdocs[i];
                List<ICar> cs = new List<ICar>();
                for (int j = 0; j < cars.Count; j++)
                {
                    Car c = cars[j] as Car;
                    if (c.CurrentWeight + d.Weight <= c.MaxWeight)
                    {
                        cs.Add(cars[j]);
                    }
                }

                if (cs.Count > 0)
                {
                    Car c = cs[0] as Car;
                    double minDistance = Math.Sqrt((c.CurrentLat - d.Lat) * (c.CurrentLat - d.Lat) +
                                                    (c.CurrentLon - d.Lon) * (c.CurrentLon - d.Lon));
                    // если подходящих по грузоподъемностимашин не одна,
                    // то определяем ближайщую
                    for (int j = 1; j < cs.Count; j++)
                    {
                        double distance = Math.Sqrt((d.Lat - c.CurrentLat) * (d.Lat - c.CurrentLat) +
                            (d.Lon - c.CurrentLon) * (d.Lon - c.CurrentLon));
                        if (distance < minDistance)
                        {
                            c = cs[j] as Car;
                            minDistance = distance;
                        }
                    }

                    // и загружаем ее(машины)
                    c.CurrentWeight += d.Weight;
                    c.CurrentLat = d.Lat;
                    c.CurrentLon = d.Lon;
                    c.Route.Docs.Add(d);
                    tdocs.Remove(d);
                    i--;
                }
            }

            // подготовка списка маршрутов в порядке
            // обработанных машин
            List<IRoute> lroute = new List<IRoute>();
            for (int i = 0; i < cars.Count; i++)
            {
                //if (cars[i].CreateRoute().Docs.Count > 0)
                {
                    lroute.Add(cars[i].CreateRoute());
                }
            }
            return lroute;
			//throw new NotImplementedException();
		}
	}
	/// <summary>
	/// Документ для доставки
	/// </summary>
	public interface IDoc
	{
		/// <summary>
		/// Географическая широта
		/// </summary>
		double Lat { get; }
		/// <summary>
		/// Географическая долгота
		/// </summary>
		double Lon { get; }
		/// <summary>
		/// Вес товара
		/// </summary>
		double Weight { get; }
	}

	/// <summary>
	/// Маршрут автомобиля
	/// </summary>
	public interface IRoute
	{
		/// <summary>
		/// Список документов в маршруте
		/// </summary>
		List<IDoc> Docs { get; set; }
	}

	/// <summary>
	/// Автомобиль для доставки
	/// </summary>
	public interface ICar
	{
		/// <summary>
		/// Начальная широта
		/// </summary>
		double StartLat { get; }
		/// <summary>
		/// Начальная долгота
		/// </summary>
		double StartLon { get; }
		/// <summary>
		/// Максимальный вес
		/// </summary>
		double MaxWeight { get; }
		/// <summary>
		/// Создать маршрут с этим автомобилем
		/// </summary>
		/// <returns></returns>
		IRoute CreateRoute();
	}

    public class Doc : IDoc
    {
        /// <summary>
        /// Географическая широта
        /// </summary>
        public double Lat { get; protected set; }
        /// <summary>
        /// Географическая долгота
        /// </summary>
        public double Lon { get; protected set; }
        /// <summary>
        /// Вес товара
        /// </summary>
        public double Weight { get; protected set; }

        public Doc(double lat, double lon, double weight)
        {
            Lat = lat;
            Lon = lon;
            Weight = weight;
        }
    }

    public class Route : IRoute
    {
        /// <summary>
        /// Список документов в маршруте
        /// </summary>
        public List<IDoc> Docs { get; set; }
        public Route()
        {
            Docs = new List<IDoc>();
        }
    }

    public class Car : ICar
    {
        /// <summary>
        /// Начальная широта
        /// </summary>
        public double StartLat { get; protected set; }
        /// <summary>
        /// Начальная долгота
        /// </summary>
        public double StartLon { get; protected set; }
        /// <summary>
        /// Максимальный вес
        /// </summary>
        public double MaxWeight { get; protected set; }

        /// <summary>
        /// Текущая загрузка
        /// </summary>
        public double CurrentWeight { get; set; }
        /// <summary>
        /// Маршрут
        /// </summary>
        public IRoute Route { get; set; }
        /// <summary>
        /// Текущая широта
        /// </summary>
        public double CurrentLat { get; set; }
        /// <summary>
        /// Текущая долгота
        /// </summary>
        public double CurrentLon { get; set; }
        /// <summary>
        /// Создать маршрут с этим автомобилем
        /// </summary>
        /// <returns></returns>
        public IRoute CreateRoute()
        {
            return Route;
        }

        public Car(double startLat, double startLon, double maxWeight)
        {
            StartLat = startLat;
            StartLon = startLon;
            MaxWeight = maxWeight;
            CurrentWeight = 0;
            CurrentLat = StartLat;
            CurrentLon = StartLon;
            Route = new Route();
        }
    }
}
