﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestProject;

namespace WindowsFormsApplicationTest
{
    public partial class TestForm : Form
    {
        private DataTable dtDocs = new DataTable();
        private DataTable dtCars = new DataTable();
        private DataTable dtRoutes = new DataTable();
        private List<IRoute> lroute;

        public TestForm()
        {
            InitializeComponent();

            // создание столбцов для таблицы грузов
            Docs.DataSource = dtDocs;
            dtDocs.Columns.Add("Lat");
            dtDocs.Columns.Add("Lon");
            dtDocs.Columns.Add("Weight");

            // создание столбцов для таблицы машин
            Cars.DataSource = dtCars;
            dtCars.Columns.Add("StartLat");
            dtCars.Columns.Add("StartLon");
            dtCars.Columns.Add("MaxWeight");
            
            // создание стобцов для таблицы маршрута
            Routes.DataSource = dtRoutes;
            dtRoutes.Columns.Add("Lat");
            dtRoutes.Columns.Add("Lon");
            dtRoutes.Columns.Add("Weight");

            // заполненение таблицы грузов
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dtDocs.NewRow();
                dr[0] = rnd.Next(100);
                dr[1] = rnd.Next(100);
                dr[2] = rnd.Next(100);
                dtDocs.Rows.Add(dr);
            }

            // заполненение таблицы машин
            for (int i = 0; i < 3; i++)
            {
                DataRow dr = dtCars.NewRow();
                dr[0] = rnd.Next(100);
                dr[1] = rnd.Next(100);
                dr[2] = 100 + rnd.Next(200);
                dtCars.Rows.Add(dr);
            }
        }

        /// <summary>
        /// Расчет маршрутов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalculate_Click(object sender, EventArgs e)
        {
            //
            List<ICar> cars = new List<ICar>();
            for (int i = 0; i < dtCars.Rows.Count; i++)
            {
                DataRow dr = dtCars.Rows[i];
                ICar c = new Car(Double.Parse(dr[0].ToString()), Double.Parse(dr[1].ToString()), Double.Parse(dr[2].ToString()));
                cars.Add(c);
            }
            List<IDoc> docs = new List<IDoc>();
            for (int i = 0; i < dtDocs.Rows.Count; i++)
            {
                DataRow dr = dtDocs.Rows[i];
                IDoc d = new Doc(Double.Parse(dr[0].ToString()), Double.Parse(dr[1].ToString()), Double.Parse(dr[2].ToString()));
                docs.Add(d);
            }
            Calculator calc = new TestProject.Calculator();
            try
            {
                lroute = calc.CalcRoutes(docs, cars);
                MessageBox.Show("Расчет маршрутов произведен.");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Введены недопустимые данные");
            }
        }

        private void Cars_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (lroute == null)
                return;

            int index = e.RowIndex;
            if (index < 0 || index >= lroute.Count)
                return;

            // заполнение таблицы маршрута для выбранной по индексу машины
            dtRoutes.Clear();
            for (int i = 0; i < lroute[index].Docs.Count; i++)
            {
                DataRow dr = dtRoutes.NewRow();
                dr[0] = lroute[index].Docs[i].Lat;
                dr[1] = lroute[index].Docs[i].Lon;
                dr[2] = lroute[index].Docs[i].Weight;
                dtRoutes.Rows.Add(dr);
            }
        }
    }
}
