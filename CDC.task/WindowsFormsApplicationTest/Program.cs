﻿
using System;
using System.Windows.Forms;
using WindowsFormsApplicationTest;

// This form demonstrates using a BindingNavigator to display 
// rows from a database query sequentially.
public class Program
{
    [STAThread]
    public static void Main()
    {
        Application.EnableVisualStyles();
        Application.Run(new TestForm());
    }
}
