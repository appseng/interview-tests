﻿namespace WindowsFormsApplicationTest
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Docs = new System.Windows.Forms.DataGridView();
            this.Cars = new System.Windows.Forms.DataGridView();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.Routes = new System.Windows.Forms.DataGridView();
            this.gbDocs = new System.Windows.Forms.GroupBox();
            this.gbCars = new System.Windows.Forms.GroupBox();
            this.gbRoutes = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.Docs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Routes)).BeginInit();
            this.gbDocs.SuspendLayout();
            this.gbCars.SuspendLayout();
            this.gbRoutes.SuspendLayout();
            this.SuspendLayout();
            // 
            // Docs
            // 
            this.Docs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Docs.Location = new System.Drawing.Point(6, 18);
            this.Docs.Name = "Docs";
            this.Docs.Size = new System.Drawing.Size(365, 150);
            this.Docs.TabIndex = 0;
            // 
            // Cars
            // 
            this.Cars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Cars.Location = new System.Drawing.Point(6, 15);
            this.Cars.MultiSelect = false;
            this.Cars.Name = "Cars";
            this.Cars.Size = new System.Drawing.Size(365, 150);
            this.Cars.TabIndex = 1;
            this.Cars.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Cars_CellDoubleClick);
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(113, 357);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(130, 23);
            this.btnCalculate.TabIndex = 2;
            this.btnCalculate.Text = "Рассчитать маршруты";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // Routes
            // 
            this.Routes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Routes.Location = new System.Drawing.Point(6, 18);
            this.Routes.Name = "Routes";
            this.Routes.Size = new System.Drawing.Size(365, 150);
            this.Routes.TabIndex = 3;
            // 
            // gbDocs
            // 
            this.gbDocs.Controls.Add(this.Docs);
            this.gbDocs.Location = new System.Drawing.Point(-1, 1);
            this.gbDocs.Name = "gbDocs";
            this.gbDocs.Size = new System.Drawing.Size(378, 174);
            this.gbDocs.TabIndex = 4;
            this.gbDocs.TabStop = false;
            this.gbDocs.Text = "ГРУЗЫ";
            // 
            // gbCars
            // 
            this.gbCars.Controls.Add(this.Cars);
            this.gbCars.Location = new System.Drawing.Point(-1, 175);
            this.gbCars.Name = "gbCars";
            this.gbCars.Size = new System.Drawing.Size(378, 176);
            this.gbCars.TabIndex = 1;
            this.gbCars.TabStop = false;
            this.gbCars.Text = "МАШИНЫ";
            // 
            // gbRoutes
            // 
            this.gbRoutes.Controls.Add(this.Routes);
            this.gbRoutes.Location = new System.Drawing.Point(-1, 386);
            this.gbRoutes.Name = "gbRoutes";
            this.gbRoutes.Size = new System.Drawing.Size(378, 176);
            this.gbRoutes.TabIndex = 5;
            this.gbRoutes.TabStop = false;
            this.gbRoutes.Text = "МАРШРУТ";
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 566);
            this.Controls.Add(this.gbRoutes);
            this.Controls.Add(this.gbCars);
            this.Controls.Add(this.gbDocs);
            this.Controls.Add(this.btnCalculate);
            this.Name = "TestForm";
            this.Text = "TestForm";
            ((System.ComponentModel.ISupportInitialize)(this.Docs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Routes)).EndInit();
            this.gbDocs.ResumeLayout(false);
            this.gbCars.ResumeLayout(false);
            this.gbRoutes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Docs;
        private System.Windows.Forms.DataGridView Cars;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.DataGridView Routes;
        private System.Windows.Forms.GroupBox gbDocs;
        private System.Windows.Forms.GroupBox gbCars;
        private System.Windows.Forms.GroupBox gbRoutes;

    }
}