﻿// <copyright file="Weather.cs" company="IP">
//    Dmitry Kuznetsov. All rights reserved.
// </copyright>
// <author>Dmitry Kuznetsov</author>
// <summary>
// Class for the main point of the applicaton.
// </summary>
namespace Weather.Net
{
    using System;
    using System.Net;
    using System.Text;
    using System.Windows.Forms;
    
    /// <summary>
    /// Implication function of the main form of the application.
    /// </summary>
    public partial class WeatherForm : Form
    {
        /// <summary>
        /// If the application is closing from the tray.
        /// </summary>
        private bool closedFromTray = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Weather" /> class.
        /// Constructor.
        /// </summary>
        public WeatherForm()
        {
            this.InitializeComponent();
            TrayNotifyIcon.ShowBalloonTip(100, "Weather.NET", "The application is working", ToolTipIcon.Info);
        }

        /// <summary>
        /// Add day.
        /// </summary>
        /// <param name="sender">Button "Add".</param>
        /// <param name="e">Event args.</param>
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DatesTreeView.Nodes.Count; i++)
            {
                Day dayW = DatesTreeView.Nodes[i].Tag as Day;
                if (dayW == null || dayW.Date == currentDate.Value.ToLongDateString())
                {
                    return;
                }
            }

            string dayOfWeak = currentDate.Value.DayOfWeek.ToString();
            /*string dayOfWeak = string.Empty;
            switch (currentDate.Value.DayOfWeek.ToString())
            {
                case "Monday":
                    dayOfWeak = "Понедельник";
                    break;
                case "Tuesday":
                    dayOfWeak = "Вторник";
                    break;
                case "Wednesday":
                    dayOfWeak = "Среда";
                    break;
                case "Thursday":
                    dayOfWeak = "Четверг";
                    break;
                case "Friday":
                    dayOfWeak = "Пятница";
                    break;
                case "Saturday":
                    dayOfWeak = "Суббота";
                    break;
                case "Sunday":
                    dayOfWeak = "Воскресенье";
                    break;
                default:
                    dayOfWeak = "Завтра";
                    break;
            }*/
            
            Day dw = new Day()
            {
                Date = currentDate.Value.ToLongDateString(),
                DayOfWeek = dayOfWeak,
                Temperature = Temperature.Text,
                WeatherKind = WeatherKind.Text,
                Pressure = TxtPressure.Text,
                Humidity = TxtHumidity.Text
            };
            
            TreeNode tn = new TreeNode(dw.Date);
            tn.Tag = dw;
            tn.EnsureVisible();
            DatesTreeView.Nodes.Add(tn);
        }

        /// <summary>
        /// Double click on a specific day from the tree-view.
        /// </summary>
        /// <param name="sender">Tree-view "Dates".</param>
        /// <param name="e">Event args.</param>
        private void DatesTreeView_DoubleClick(object sender, EventArgs e)
        {
            TreeView tv = (TreeView)sender;
            if (tv == null)
            {
                return;
            }

            TreeNode tn = tv.SelectedNode;
            if (tn == null)
            {
                return;
            }

            Day dw = (Day)tn.Tag;
            if (dw == null)
            {
                return;
            }

            SpecificDay sd = new SpecificDay(dw);
            sd.ShowDialog();
        }

        /// <summary>
        /// Loading the data from the Internet.
        /// </summary>
        /// <param name="sender">Button "LoadWeather".</param>
        /// <param name="e">Event args.</param>
        private void BtnLoadWeather_Click(object sender, EventArgs e)
        {
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("UserAgent", "Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.02");
                string site = @"http://pogoda.yandex.ru/moscow/details";
                string content = client.DownloadString(site);

                content = content.Substring(content.IndexOf("<table cellspacing=\"0\" class=\"b-forecast-detailed__data\">"));
                byte[] bs = Encoding.Default.GetBytes(content);
                content = Encoding.UTF8.GetString(bs);

                string subStr = string.Empty;
                string value0 = "<div class=\"b-forecast-detailed__value b-forecast-detailed__value_padding_dayname\">";
                string value05 = "<div class=\"b-forecast-detailed__value b-forecast-detailed__value_padding_dayname b-forecast-detailed__value_weekend_yes\">";
                string valueDiv = "</div>";
                string value1 = "<div class=\"b-forecast-detailed__date\">";
                string value15 = "<div class=\"b-forecast-detailed__month\">";
                int start0 = value0.Length;
                int start05 = value05.Length;
                int start1 = value1.Length;
                int start15 = value15.Length;

                string value2 = "<div class=\"b-forecast-detailed__daypart\">днем</div>";
                string value25 = "<div class=\"b-forecast-detailed__temp\">";
                int start25 = value25.Length;

                string value3 = "<div class=\"b-forecast-detailed__value\">";
                int start3 = value3.Length;
                int end = 0;

                DatesTreeView.Nodes.Clear();

                for (int i = 0; i < 10; i++)
                {
                    int j = content.IndexOf(value0);
                    int k = content.IndexOf(value05);
                    if ((j == -1 || j > k) && k != -1)
                    {
                        content = content.Substring(k);
                        end = content.IndexOf(valueDiv);
                        subStr = content.Substring(start05, end - start05);
                    }
                    else
                    {
                        content = content.Substring(j);
                        end = content.IndexOf(valueDiv);
                        subStr = content.Substring(start0, end - start0);
                    }

                    Day dw = new Day();
                    switch (subStr)
                    {
                        case "пн":
                            dw.DayOfWeek = "Monday";
                            break;
                        case "вт":
                            dw.DayOfWeek = "Tuesday";
                            break;
                        case "ср":
                            dw.DayOfWeek = "Wednesday";
                            break;
                        case "чт":
                            dw.DayOfWeek = "Thursday";
                            break;
                        case "пт":
                            dw.DayOfWeek = "Friday";
                            break;
                        case "сб":
                            dw.DayOfWeek = "Saturday";
                            break;
                        case "вс":
                            dw.DayOfWeek = "Sunday";
                            break;
                        default:
                            dw.DayOfWeek = "Tomorrow";
                            break;
                    }

                    content = content.Substring(content.IndexOf(value1));
                    end = content.IndexOf(valueDiv);
                    subStr = content.Substring(start1, end - start1);
                    string tomorrow = "<a name=\"tomorrow\">";
                    int m = subStr.IndexOf(tomorrow);
                    if (m != -1)
                    {
                        end = subStr.IndexOf("</a>");
                        int start09 = tomorrow.Length;
                        subStr = subStr.Substring(start09, end - start09);
                    }
                    
                    content = content.Substring(content.IndexOf(value15));
                    end = content.IndexOf(valueDiv);
                    subStr += " " + content.Substring(start15, end - start15);
                    dw.Date = subStr;

                    content = content.Substring(content.IndexOf(value2));
                    content = content.Substring(content.IndexOf(value25));
                    end = content.IndexOf(valueDiv);
                    subStr = content.Substring(start25, end - start25);
                    subStr = subStr.Replace("…", "...");
                    dw.Temperature = subStr;

                    content = content.Substring(content.IndexOf(value3));
                    end = content.IndexOf(valueDiv);
                    content = content.Substring(end);
                    content = content.Substring(content.IndexOf(value3));
                    end = content.IndexOf(valueDiv);
                    subStr = content.Substring(start3, end - start3);
                    dw.WeatherKind = subStr;

                    content = content.Substring(end);
                    content = content.Substring(content.IndexOf(value3));
                    end = content.IndexOf(valueDiv);
                    subStr = content.Substring(start3, end - start3);
                    dw.Pressure = subStr;

                    content = content.Substring(end);
                    content = content.Substring(content.IndexOf(value3));
                    end = content.IndexOf(valueDiv);
                    subStr = content.Substring(start3, end - start3);
                    dw.Humidity = subStr;

                    TreeNode tn = new TreeNode(dw.Date);
                    tn.Tag = dw;
                    DatesTreeView.Nodes.Add(tn);
                }
                
                TrayNotifyIcon.ShowBalloonTip(100, "Weather.NET", "Weather loaded", ToolTipIcon.Info);
            }
            catch
            {
                TrayNotifyIcon.ShowBalloonTip(100, "Weather.NET", "Error occured", ToolTipIcon.Error);
            }
        }

        /// <summary>
        /// Delete specific day from the tree-view.
        /// </summary>
        /// <param name="sender">Button "Delete".</param>
        /// <param name="e">Event args.</param>
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (DatesTreeView.SelectedNode == null)
            {
                return;
            }

            DatesTreeView.Nodes.Remove(DatesTreeView.SelectedNode);
        }

        /// <summary>
        /// Show context menu.
        /// </summary>
        /// <param name="sender">A notify icon.</param>
        /// <param name="e">Event args.</param>
        private void TrayNotifyIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                TrayNotifyIcon.ContextMenuStrip.Show(Cursor.Position.X, Cursor.Position.Y - NotifyIconContextMenuStrip.Height);
            }
        }

        /// <summary>
        /// Quit the application.
        /// </summary>
        /// <param name="sender">A menu item "Exit".</param>
        /// <param name="e">Event args.</param>
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closedFromTray = true;
            this.Close();
        }

        /// <summary>
        /// Hide the form of the application.
        /// </summary>
        /// <param name="sender">A menu item "Hide".</param>
        /// <param name="e">Event args.</param>
        private void HideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            HideToolStripMenuItem.Enabled = false;
            ShowToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// Show the main form of the application.
        /// </summary>
        /// <param name="sender">A menu item "Show".</param>
        /// <param name="e">Event args.</param>
        private void ShowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                this.Show();
                HideToolStripMenuItem.Enabled = true;
                ShowToolStripMenuItem.Enabled = false;
            }
        }

        /// <summary>
        /// Closing of the application.
        /// </summary>
        /// <param name="sender">A form "Weather".</param>
        /// <param name="e">Event args.</param>
        private void Weather_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.closedFromTray && e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.HideToolStripMenuItem_Click(sender, e);
            }
        }
    }
}
