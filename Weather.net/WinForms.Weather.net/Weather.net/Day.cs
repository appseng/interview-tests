﻿// <copyright file="Day.cs" company="IP">
//    Dmitry Kuznetsov. All rights reserved.
// </copyright>
// <author>Dmitry Kuznetsov</author>
// <summary>
// Class for storing information about one day.
// </summary>
namespace Weather.Net
{
    using System;

    /// <summary>
    /// Information about weather on particular day.
    /// </summary>
    public class Day
    {
        /// <summary>
        /// Gets or sets temperature.
        /// </summary>
        /// <value>String with temperature.</value>
        public string Temperature { get; set; }
        
        /// <summary>
        /// Gets or sets date.
        /// </summary>
        /// <value>String with date.</value>
        public string Date { get; set; }
        
        /// <summary>
        /// Gets or sets a day of a week.
        /// </summary>
        /// <value>String with the day of the week.</value>
        public string DayOfWeek { get; set; }
        
        /// <summary>
        /// Gets or sets a type of weather.
        /// </summary>
        /// <value>String with the type of the weather.</value>
        public string WeatherKind { get; set; }
        
        /// <summary>
        /// Gets or sets pressure.
        /// </summary>
        /// <value>String with the pressure.</value>
        public string Pressure { get; set; }
        
        /// <summary>
        /// Gets or sets humidity.
        /// </summary>
        /// <value>String with the humidity.</value>
        public string Humidity { get; set; }
    }
}
