﻿// <copyright file="Program.cs" company="IP">
//    Dmitry Kuznetsov. All rights reserved.
// </copyright>
// <author>Dmitry Kuznetsov</author>
// <summary>
// Class for the main point of the applicaton.
// </summary>
namespace Weather.Net
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// Class with the main entry point.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WeatherForm());
        }
    }
}
