﻿namespace Weather.Net
{
    partial class SpecificDay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.PictureWeatherKind = new System.Windows.Forms.PictureBox();
        	this.txtDayOfWeek = new System.Windows.Forms.TextBox();
        	this.txtWeatherKind = new System.Windows.Forms.TextBox();
        	this.txtDate = new System.Windows.Forms.TextBox();
        	this.txtTemperature = new System.Windows.Forms.TextBox();
        	this.txtPressure = new System.Windows.Forms.TextBox();
        	this.txtHumidity = new System.Windows.Forms.TextBox();
        	this.LblHumidity = new System.Windows.Forms.Label();
        	this.label1 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	((System.ComponentModel.ISupportInitialize)(this.PictureWeatherKind)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// PictureWeatherKind
        	// 
        	this.PictureWeatherKind.Image = global::Weather.Net.Properties.WeatherKinds.partly_cloudy_with_a_chance_of_rain_thunderstorm;
        	this.PictureWeatherKind.InitialImage = null;
        	this.PictureWeatherKind.Location = new System.Drawing.Point(26, 46);
        	this.PictureWeatherKind.Name = "PictureWeatherKind";
        	this.PictureWeatherKind.Size = new System.Drawing.Size(100, 35);
        	this.PictureWeatherKind.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
        	this.PictureWeatherKind.TabIndex = 2;
        	this.PictureWeatherKind.TabStop = false;
        	// 
        	// txtDayOfWeek
        	// 
        	this.txtDayOfWeek.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.txtDayOfWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.txtDayOfWeek.Location = new System.Drawing.Point(26, 5);
        	this.txtDayOfWeek.Name = "txtDayOfWeek";
        	this.txtDayOfWeek.ReadOnly = true;
        	this.txtDayOfWeek.Size = new System.Drawing.Size(100, 13);
        	this.txtDayOfWeek.TabIndex = 0;
        	this.txtDayOfWeek.Text = "Sunday";
        	this.txtDayOfWeek.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	// 
        	// txtWeatherKind
        	// 
        	this.txtWeatherKind.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.txtWeatherKind.Location = new System.Drawing.Point(26, 85);
        	this.txtWeatherKind.Multiline = true;
        	this.txtWeatherKind.Name = "txtWeatherKind";
        	this.txtWeatherKind.ReadOnly = true;
        	this.txtWeatherKind.Size = new System.Drawing.Size(100, 58);
        	this.txtWeatherKind.TabIndex = 2;
        	this.txtWeatherKind.Text = "partly cloudy with a chance of rain, thunderstorm";
        	this.txtWeatherKind.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	// 
        	// txtDate
        	// 
        	this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.txtDate.Location = new System.Drawing.Point(-2, 24);
        	this.txtDate.Name = "txtDate";
        	this.txtDate.ReadOnly = true;
        	this.txtDate.Size = new System.Drawing.Size(151, 16);
        	this.txtDate.TabIndex = 1;
        	this.txtDate.Text = "14 October 2012 г.";
        	this.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	// 
        	// txtTemperature
        	// 
        	this.txtTemperature.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.txtTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.txtTemperature.Location = new System.Drawing.Point(26, 158);
        	this.txtTemperature.Name = "txtTemperature";
        	this.txtTemperature.ReadOnly = true;
        	this.txtTemperature.Size = new System.Drawing.Size(100, 19);
        	this.txtTemperature.TabIndex = 3;
        	this.txtTemperature.Text = "+12...+14";
        	this.txtTemperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	// 
        	// txtPressure
        	// 
        	this.txtPressure.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.txtPressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.txtPressure.Location = new System.Drawing.Point(26, 196);
        	this.txtPressure.Name = "txtPressure";
        	this.txtPressure.ReadOnly = true;
        	this.txtPressure.Size = new System.Drawing.Size(100, 19);
        	this.txtPressure.TabIndex = 4;
        	this.txtPressure.Text = "731";
        	this.txtPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	// 
        	// txtHumidity
        	// 
        	this.txtHumidity.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.txtHumidity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.txtHumidity.Location = new System.Drawing.Point(26, 237);
        	this.txtHumidity.Name = "txtHumidity";
        	this.txtHumidity.ReadOnly = true;
        	this.txtHumidity.Size = new System.Drawing.Size(100, 19);
        	this.txtHumidity.TabIndex = 5;
        	this.txtHumidity.Text = "69%";
        	this.txtHumidity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	// 
        	// LblHumidity
        	// 
        	this.LblHumidity.AutoSize = true;
        	this.LblHumidity.Location = new System.Drawing.Point(47, 221);
        	this.LblHumidity.Name = "LblHumidity";
        	this.LblHumidity.Size = new System.Drawing.Size(53, 13);
        	this.LblHumidity.TabIndex = 12;
        	this.LblHumidity.Text = "Humidity :";
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(47, 180);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(54, 13);
        	this.label1.TabIndex = 13;
        	this.label1.Text = "Pressure :";
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(38, 142);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(73, 13);
        	this.label2.TabIndex = 14;
        	this.label2.Text = "Temperature :";
        	// 
        	// SpecificDay
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(148, 263);
        	this.Controls.Add(this.label2);
        	this.Controls.Add(this.label1);
        	this.Controls.Add(this.LblHumidity);
        	this.Controls.Add(this.txtHumidity);
        	this.Controls.Add(this.txtPressure);
        	this.Controls.Add(this.txtTemperature);
        	this.Controls.Add(this.txtDate);
        	this.Controls.Add(this.txtWeatherKind);
        	this.Controls.Add(this.txtDayOfWeek);
        	this.Controls.Add(this.PictureWeatherKind);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        	this.Name = "SpecificDay";
        	this.Text = "Sunday";
        	((System.ComponentModel.ISupportInitialize)(this.PictureWeatherKind)).EndInit();
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.PictureBox PictureWeatherKind;
        private System.Windows.Forms.TextBox txtDayOfWeek;
        private System.Windows.Forms.TextBox txtWeatherKind;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtTemperature;
        private System.Windows.Forms.TextBox txtPressure;
        private System.Windows.Forms.TextBox txtHumidity;
        private System.Windows.Forms.Label LblHumidity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}