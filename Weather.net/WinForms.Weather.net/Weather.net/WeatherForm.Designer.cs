﻿namespace Weather.Net
{
    partial class WeatherForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeatherForm));
        	this.LblWeatherType = new System.Windows.Forms.Label();
        	this.LblDate = new System.Windows.Forms.Label();
        	this.LblTemperature = new System.Windows.Forms.Label();
        	this.Temperature = new System.Windows.Forms.TextBox();
        	this.currentDate = new System.Windows.Forms.DateTimePicker();
        	this.BtnLoadWeather = new System.Windows.Forms.Button();
        	this.BtnAdd = new System.Windows.Forms.Button();
        	this.WeatherKind = new System.Windows.Forms.ComboBox();
        	this.DatesTreeView = new System.Windows.Forms.TreeView();
        	this.BtnDelete = new System.Windows.Forms.Button();
        	this.LblPressure = new System.Windows.Forms.Label();
        	this.TxtPressure = new System.Windows.Forms.TextBox();
        	this.LblHumidity = new System.Windows.Forms.Label();
        	this.TxtHumidity = new System.Windows.Forms.TextBox();
        	this.TrayNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
        	this.NotifyIconContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
        	this.ShowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.HideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        	this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.NotifyIconContextMenuStrip.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// LblWeatherType
        	// 
        	this.LblWeatherType.AutoSize = true;
        	this.LblWeatherType.Location = new System.Drawing.Point(161, 150);
        	this.LblWeatherType.Name = "LblWeatherType";
        	this.LblWeatherType.Size = new System.Drawing.Size(77, 13);
        	this.LblWeatherType.TabIndex = 18;
        	this.LblWeatherType.Text = "Weather kind :";
        	// 
        	// LblDate
        	// 
        	this.LblDate.AutoSize = true;
        	this.LblDate.Location = new System.Drawing.Point(162, 33);
        	this.LblDate.Name = "LblDate";
        	this.LblDate.Size = new System.Drawing.Size(36, 13);
        	this.LblDate.TabIndex = 17;
        	this.LblDate.Text = "Date :";
        	// 
        	// LblTemperature
        	// 
        	this.LblTemperature.AutoSize = true;
        	this.LblTemperature.Location = new System.Drawing.Point(161, 58);
        	this.LblTemperature.Name = "LblTemperature";
        	this.LblTemperature.Size = new System.Drawing.Size(73, 13);
        	this.LblTemperature.TabIndex = 16;
        	this.LblTemperature.Text = "Temperature :";
        	// 
        	// Temperature
        	// 
        	this.Temperature.Location = new System.Drawing.Point(250, 55);
        	this.Temperature.Name = "Temperature";
        	this.Temperature.Size = new System.Drawing.Size(100, 20);
        	this.Temperature.TabIndex = 2;
        	this.Temperature.Text = "+12";
        	this.Temperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	// 
        	// currentDate
        	// 
        	this.currentDate.Location = new System.Drawing.Point(207, 29);
        	this.currentDate.Name = "currentDate";
        	this.currentDate.Size = new System.Drawing.Size(143, 20);
        	this.currentDate.TabIndex = 1;
        	// 
        	// BtnLoadWeather
        	// 
        	this.BtnLoadWeather.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.BtnLoadWeather.Location = new System.Drawing.Point(335, 245);
        	this.BtnLoadWeather.Name = "BtnLoadWeather";
        	this.BtnLoadWeather.Size = new System.Drawing.Size(114, 23);
        	this.BtnLoadWeather.TabIndex = 8;
        	this.BtnLoadWeather.Text = "Load from the Web";
        	this.BtnLoadWeather.UseVisualStyleBackColor = true;
        	this.BtnLoadWeather.Click += new System.EventHandler(this.BtnLoadWeather_Click);
        	// 
        	// BtnAdd
        	// 
        	this.BtnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
        	this.BtnAdd.Location = new System.Drawing.Point(165, 245);
        	this.BtnAdd.Name = "BtnAdd";
        	this.BtnAdd.Size = new System.Drawing.Size(75, 23);
        	this.BtnAdd.TabIndex = 6;
        	this.BtnAdd.Text = "Add";
        	this.BtnAdd.UseVisualStyleBackColor = true;
        	this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
        	// 
        	// WeatherKind
        	// 
        	this.WeatherKind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.WeatherKind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.WeatherKind.FormattingEnabled = true;
        	this.WeatherKind.Items.AddRange(new object[] {
        	        	        	"clear",
        	        	        	"clear, no precipitation",
        	        	        	"cloudy",
        	        	        	"cloudy with light snow",
        	        	        	"cloudy, light rain",
        	        	        	"cloudy, light snow",
        	        	        	"cloudy, rain",
        	        	        	"cloudy, snow showers",
        	        	        	"mostly cloudy",
        	        	        	"mostly cloudy, rain",
        	        	        	"partly cloudy",
        	        	        	"partly cloudy with a chance of rain, thunderstorm",
        	        	        	"partly cloudy, light rain",
        	        	        	"partly sunny"});
        	this.WeatherKind.Location = new System.Drawing.Point(165, 175);
        	this.WeatherKind.Name = "WeatherKind";
        	this.WeatherKind.Size = new System.Drawing.Size(284, 21);
        	this.WeatherKind.Sorted = true;
        	this.WeatherKind.TabIndex = 5;
        	// 
        	// DatesTreeView
        	// 
        	this.DatesTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left)));
        	this.DatesTreeView.Location = new System.Drawing.Point(8, 12);
        	this.DatesTreeView.Name = "DatesTreeView";
        	this.DatesTreeView.Size = new System.Drawing.Size(151, 256);
        	this.DatesTreeView.TabIndex = 10;
        	this.DatesTreeView.DoubleClick += new System.EventHandler(this.DatesTreeView_DoubleClick);
        	// 
        	// BtnDelete
        	// 
        	this.BtnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
        	this.BtnDelete.Location = new System.Drawing.Point(246, 245);
        	this.BtnDelete.Name = "BtnDelete";
        	this.BtnDelete.Size = new System.Drawing.Size(75, 23);
        	this.BtnDelete.TabIndex = 7;
        	this.BtnDelete.Text = "Delete";
        	this.BtnDelete.UseVisualStyleBackColor = true;
        	this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
        	// 
        	// LblPressure
        	// 
        	this.LblPressure.AutoSize = true;
        	this.LblPressure.Location = new System.Drawing.Point(162, 87);
        	this.LblPressure.Name = "LblPressure";
        	this.LblPressure.Size = new System.Drawing.Size(54, 13);
        	this.LblPressure.TabIndex = 21;
        	this.LblPressure.Text = "Pressure :";
        	// 
        	// TxtPressure
        	// 
        	this.TxtPressure.Location = new System.Drawing.Point(250, 84);
        	this.TxtPressure.Name = "TxtPressure";
        	this.TxtPressure.Size = new System.Drawing.Size(100, 20);
        	this.TxtPressure.TabIndex = 3;
        	this.TxtPressure.Text = "731";
        	this.TxtPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	// 
        	// LblHumidity
        	// 
        	this.LblHumidity.AutoSize = true;
        	this.LblHumidity.Location = new System.Drawing.Point(162, 113);
        	this.LblHumidity.Name = "LblHumidity";
        	this.LblHumidity.Size = new System.Drawing.Size(53, 13);
        	this.LblHumidity.TabIndex = 23;
        	this.LblHumidity.Text = "Humidity :";
        	// 
        	// TxtHumidity
        	// 
        	this.TxtHumidity.Location = new System.Drawing.Point(250, 110);
        	this.TxtHumidity.Name = "TxtHumidity";
        	this.TxtHumidity.Size = new System.Drawing.Size(100, 20);
        	this.TxtHumidity.TabIndex = 4;
        	this.TxtHumidity.Text = "69%";
        	this.TxtHumidity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	// 
        	// TrayNotifyIcon
        	// 
        	this.TrayNotifyIcon.ContextMenuStrip = this.NotifyIconContextMenuStrip;
        	this.TrayNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayNotifyIcon.Icon")));
        	this.TrayNotifyIcon.Text = "Wether.NET";
        	this.TrayNotifyIcon.Visible = true;
        	this.TrayNotifyIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrayNotifyIcon_MouseUp);
        	// 
        	// NotifyIconContextMenuStrip
        	// 
        	this.NotifyIconContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.ShowToolStripMenuItem,
        	        	        	this.HideToolStripMenuItem,
        	        	        	this.ToolStripSeparator1,
        	        	        	this.ExitToolStripMenuItem});
        	this.NotifyIconContextMenuStrip.Name = "NotifyIconContextMenuStrip";
        	this.NotifyIconContextMenuStrip.ShowImageMargin = false;
        	this.NotifyIconContextMenuStrip.Size = new System.Drawing.Size(109, 76);
        	// 
        	// ShowToolStripMenuItem
        	// 
        	this.ShowToolStripMenuItem.Enabled = false;
        	this.ShowToolStripMenuItem.Name = "ShowToolStripMenuItem";
        	this.ShowToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
        	this.ShowToolStripMenuItem.Text = "Показать";
        	this.ShowToolStripMenuItem.Click += new System.EventHandler(this.ShowToolStripMenuItem_Click);
        	// 
        	// HideToolStripMenuItem
        	// 
        	this.HideToolStripMenuItem.Name = "HideToolStripMenuItem";
        	this.HideToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
        	this.HideToolStripMenuItem.Text = "Скрыть";
        	this.HideToolStripMenuItem.Click += new System.EventHandler(this.HideToolStripMenuItem_Click);
        	// 
        	// ToolStripSeparator1
        	// 
        	this.ToolStripSeparator1.Name = "ToolStripSeparator1";
        	this.ToolStripSeparator1.Size = new System.Drawing.Size(105, 6);
        	// 
        	// ExitToolStripMenuItem
        	// 
        	this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
        	this.ExitToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
        	this.ExitToolStripMenuItem.Text = "Выход";
        	this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
        	// 
        	// WeatherForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(457, 280);
        	this.Controls.Add(this.LblHumidity);
        	this.Controls.Add(this.TxtHumidity);
        	this.Controls.Add(this.LblPressure);
        	this.Controls.Add(this.TxtPressure);
        	this.Controls.Add(this.BtnDelete);
        	this.Controls.Add(this.LblWeatherType);
        	this.Controls.Add(this.LblDate);
        	this.Controls.Add(this.LblTemperature);
        	this.Controls.Add(this.Temperature);
        	this.Controls.Add(this.currentDate);
        	this.Controls.Add(this.BtnLoadWeather);
        	this.Controls.Add(this.BtnAdd);
        	this.Controls.Add(this.WeatherKind);
        	this.Controls.Add(this.DatesTreeView);
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.MinimumSize = new System.Drawing.Size(465, 314);
        	this.Name = "WeatherForm";
        	this.ShowIcon = false;
        	this.Text = "Weather in Moscow";
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Weather_FormClosing);
        	this.NotifyIconContextMenuStrip.ResumeLayout(false);
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label LblWeatherType;
        private System.Windows.Forms.Label LblDate;
        private System.Windows.Forms.Label LblTemperature;
        private System.Windows.Forms.TextBox Temperature;
        private System.Windows.Forms.DateTimePicker currentDate;
        private System.Windows.Forms.Button BtnLoadWeather;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.ComboBox WeatherKind;
        private System.Windows.Forms.TreeView DatesTreeView;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Label LblPressure;
        private System.Windows.Forms.TextBox TxtPressure;
        private System.Windows.Forms.Label LblHumidity;
        private System.Windows.Forms.TextBox TxtHumidity;
        private System.Windows.Forms.NotifyIcon TrayNotifyIcon;
        private System.Windows.Forms.ContextMenuStrip NotifyIconContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HideToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ShowToolStripMenuItem;

    }
}

