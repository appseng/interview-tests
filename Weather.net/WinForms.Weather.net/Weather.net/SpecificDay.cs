﻿// <copyright file="SpecificDay.cs" company="IP">
//    Dmitry Kuznetsov. All rights reserved.
// </copyright>
// <author>Dmitry Kuznetsov</author>
// <summary>
// Class for the main point of the applicaton.
// </summary>
namespace Weather.Net
{
    using System;
    using System.Text;
    using System.Windows.Forms;
    using Weather.Net.Properties;
    
    /// <summary>
    /// Writing info on the form.
    /// </summary>
    public partial class SpecificDay : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificDay" /> class.
        /// Constructor for initial configuration of this form.
        /// </summary>
        public SpecificDay()
        {
            this.InitializeComponent();
            this.ShowInTaskbar = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificDay" /> class.
        /// Loading Day-class in the form.
        /// </summary>
        /// <param name="dw">Class Day.</param>
        public SpecificDay(Day dw)
            : this()
        {
            txtDayOfWeek.Text = this.Text = dw.DayOfWeek;
            txtDate.Text = dw.Date;
            txtTemperature.Text = dw.Temperature;
            txtWeatherKind.Text = dw.WeatherKind;
            txtPressure.Text = dw.Pressure;
            txtHumidity.Text = dw.Humidity;
            switch (dw.WeatherKind)
            {
            	case "малооблачно":
                case "partly sunny":
            		PictureWeatherKind.Image = WeatherKinds.partly_sunny;
                    break;
            	case "малооблачно, без существенных осадков":
                case "clear, no precipitation":
                    PictureWeatherKind.Image = WeatherKinds.clear_no_precipitation;
                    break;
            	case "облачно":
                case "cloudy":
                    PictureWeatherKind.Image = WeatherKinds.cloudy;
                    break;
            	case "облачно с прояснениями":
                case "partly cloudy":
                    PictureWeatherKind.Image = WeatherKinds.partly_cloudy;
                    break;
            	case "облачно с прояснениями, небольшой дождь":
                case "mostly cloudy, rain":
                    PictureWeatherKind.Image = WeatherKinds.mostly_cloudy_rain;
                    break;
            	case "облачно, временами снег":
                case "cloudy, snow showers":
                    PictureWeatherKind.Image = WeatherKinds.cloudy_snow_showers;
                    break;
            	case "облачно, дождь":
                case "cloudy, rain":
                    PictureWeatherKind.Image = WeatherKinds.cloudy_rain;
                    break;
            	case "облачно, небольшой дождь":
                case "cloudy, light rain":
                    PictureWeatherKind.Image = WeatherKinds.cloudy_light_rain;
                    break;
            	case "облачно, небольшой снег":
                case "cloudy with light snow":
                    PictureWeatherKind.Image = WeatherKinds.cloudy_with_light_snow;
                    break;
            	case "переменная облачность":
                case "mostly cloudy":
                    PictureWeatherKind.Image = WeatherKinds.mostly_cloudy;
                    break;
            	case "переменная облачность, возможен дождь, гроза":
                case "partly cloudy with a chance of rain, thunderstorm":
                    PictureWeatherKind.Image = WeatherKinds.partly_cloudy_with_a_chance_of_rain_thunderstorm;
                    break;
            	case "переменная облачность, небольшой дождь":
                case "partly cloudy, light rain":
                    PictureWeatherKind.Image = WeatherKinds.partly_cloudy_light_rain;
                    break;
            	case "переменная облачность, небольшой снег":
                case "cloudy, light snow":
                    PictureWeatherKind.Image = WeatherKinds.cloudy_light_snow;
                    break;
            	case "ясно":
                case "clear":
                    PictureWeatherKind.Image = WeatherKinds.clear;
                    break;
                default:
                    break;
            }
        }
    }
}
