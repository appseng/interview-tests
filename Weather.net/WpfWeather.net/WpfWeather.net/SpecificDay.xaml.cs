﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfWeather.net
{
    /// <summary>
    /// Логика взаимодействия для SpecificDay.xaml
    /// </summary>
    public partial class SpecificDay : Window
    {
        public SpecificDay()
        {
            InitializeComponent();
        }
        public SpecificDay(DayWeather dw)
            : this()
        {
            txtDayOfWeek.Text = this.Title = dw.DayOfWeek;
            txtDate.Text = dw.Date;
            txtTemperature.Text = dw.Temperature;
            txtWeatherKind.Text = dw.WeatherKind;
            txtPressure.Text = dw.Pressure;
            txtHumidity.Text = dw.Humidity;
            BitmapImage logo = new BitmapImage();
            logo.BeginInit();
            logo.UriSource = new Uri(@"/WpfWeather.net;component/kind/" + dw.WeatherKind+".png", UriKind.RelativeOrAbsolute);
            //switch (dw.WeatherKind)
            //{
            //    case "малооблачно":
            //        logo.UriSource = new Uri(@"/WpfWeather.net;component/kind/малооблачно.png");
            //        //PictureWeatherKind.Source = global::WpfWeather.net.Properties.Resources.малооблачно;
            //        break;
            //    case "малооблачно, без существенных осадков":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.малооблачно__без_существенных_осадков;
            //        break;
            //    case "облачно":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.облачно;
            //        break;
            //    case "облачно с прояснениями":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.облачно_с_прояснениями;
            //        break;
            //    case "облачно с прояснениями, небольшой дождь":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.облачно_с_прояснениями__небольшой_дождь;
            //        break;
            //    case "облачно, временами снег":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.облачно__временами_снег;
            //        break;
            //    case "облачно, дождь":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.облачно__дождь;
            //        break;
            //    case "облачно, небольшой дождь":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.облачно__небольшой_дождь;
            //        break;
            //    case "облачно, небольшой снег":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.облачно__небольшой_снег;
            //        break;
            //    case "переменная облачность":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.переменная_облачность;
            //        break;
            //    case "переменная облачность, возможен дождь, гроза":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.переменная_облачность__возможен_дождь__гроза;
            //        break;
            //    case "переменная облачность, небольшой дождь":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.переменная_облачность__небольшой_дождь;
            //        break;
            //    case "переменная облачность, небольшой снег":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.переменная_облачность__небольшой_снег;
            //        break;
            //    case "ясно":
            //        //PictureWeatherKind.Image = global::Weather.net.Properties.Resources.ясно;
            //        break;
            //    default:
            //        break;
            //}
            logo.EndInit();
            PictureWeatherKind.Source = logo;
        }
    }
}
