﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;

namespace WpfWeather.net
{

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //List<DayWeather> WeatherList = new List<DayWeather>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < DatesTreeView.Items.Count; i++)
            {
                DayWeather DayW = ((TreeViewItem)DatesTreeView.Items[i]).Tag as DayWeather;
                if (DayW == null || !currentDate.SelectedDate.HasValue || DayW.Date == currentDate.SelectedDate.Value.ToLongDateString())
                    return;
            }

            string Date = string.Empty;
            if (!currentDate.SelectedDate.HasValue)
                return;

            switch (currentDate.SelectedDate.Value.DayOfWeek.ToString())
            {
                case "Monday":
                    Date = "Понедельник";
                    break;
                case "Tuesday":
                    Date = "Вторник";
                    break;
                case "Wednesday":
                    Date = "Среда";
                    break;
                case "Thursday":
                    Date = "Четверг";
                    break;
                case "Friday":
                    Date = "Пятница";
                    break;
                case "Saturday":
                    Date = "Суббота";
                    break;
                case "Sunday":
                    Date = "Воскресенье";
                    break;
                default:
                    Date = "Завтра";
                    break;
            }
            DayWeather DW = new DayWeather() {
                Date = currentDate.SelectedDate.Value.ToLongDateString(),
                DayOfWeek = Date,
                Temperature = txtTemperature.Text,
                WeatherKind = weatherKind.Text,
                Pressure = txtPressure.Text,
                Humidity = txtHumidity.Text
            };

            //WeatherList.Add(DW);

            TreeViewItem TN = new TreeViewItem();
            TN.Header = DW.Date;
            TN.Tag = DW;
            //TN.EnsureVisible();
            DatesTreeView.Items.Add(TN);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem tv = DatesTreeView.SelectedItem as TreeViewItem;
            if (tv == null)
                return;

            //string ss = tv.Header.ToString();
            //for (int i = 0; i < WeatherList.Count; i++)
            //{
            //    if (WeatherList[i].Date == ss)
            //    {
            //        WeatherList.RemoveAt(i);
            //        DatesTreeView.Items.Remove(DatesTreeView.SelectedItem);
            //        break;
            //    }
            //}
            //WeatherList.Remove((DayWeather)tv.Tag);
            DatesTreeView.Items.Remove(tv);
        }

        private void DatesTreeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TreeView TV = (TreeView)sender;
            if (TV == null)
                return;

            TreeViewItem TN = (TreeViewItem)TV.SelectedItem;
            if (TN == null)
                return;

            //for (int i = 0; i < WeatherList.Count; i++)
            //{
            //    if (WeatherList[i].Date == TN.Header)
            //    {
            //        DayWeather DW = WeatherList[i];
            //        SpecificDay SD = new SpecificDay(DW);
            //        SD.ShowDialog();
            //        break;
            //    }
            //}
            DayWeather DW = (DayWeather)TN.Tag;
            if (DW == null)
                return;

            SpecificDay SD = new SpecificDay(DW);
            SD.ShowDialog();
        }

        private void btnLoadWeather_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WebClient Client = new WebClient();
                Client.Headers.Add("UserAgent", "Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.289 Version/12.02");
                string HTTPSite = @"http://pogoda.yandex.ru/moscow/details";
                string Content = Client.DownloadString(HTTPSite);

                Content = Content.Substring(Content.IndexOf("<table cellspacing=\"0\" class=\"b-forecast-detailed__data\">"));
                byte[] bs = Encoding.Default.GetBytes(Content);
                Content = Encoding.UTF8.GetString(bs);

                string SubStr = string.Empty;
                string Value0 = "<div class=\"b-forecast-detailed__value b-forecast-detailed__value_padding_dayname\">";
                string Value05 = "<div class=\"b-forecast-detailed__value b-forecast-detailed__value_padding_dayname b-forecast-detailed__value_weekend_yes\">";
                string ValueDiv = "</div>";
                string Value1 = "<div class=\"b-forecast-detailed__date\">";
                string Value15 = "<div class=\"b-forecast-detailed__month\">";
                int Start0 = Value0.Length;
                int Start05 = Value05.Length;
                int Start1 = Value1.Length;
                int Start15 = Value15.Length;

                string Value2 = "<div class=\"b-forecast-detailed__daypart\">днем</div>";
                string Value25 = "<div class=\"b-forecast-detailed__temp\">";
                int Start25 = Value25.Length;

                string Value3 = "<div class=\"b-forecast-detailed__value\">";
                int Start3 = Value3.Length;
                int End = 0;

                //WeatherList.Clear();
                DatesTreeView.Items.Clear();

                for (int i = 0; i < 10; i++)
                {
                    int j = Content.IndexOf(Value0);
                    int k = Content.IndexOf(Value05);
                    if ((j ==-1 || j > k) && k != -1 )
                    {
                        Content = Content.Substring(k);
                        End = Content.IndexOf(ValueDiv);
                        SubStr = Content.Substring(Start05, End - Start05);
                    }
                    else
                    {
                        Content = Content.Substring(j);
                        End = Content.IndexOf(ValueDiv);
                        SubStr = Content.Substring(Start0, End - Start0);
                    }

                    DayWeather DW = new DayWeather();
                    switch (SubStr)
                    {
                        case "пн":
                            DW.DayOfWeek = "Понедельник";
                            break;
                        case "вт":
                            DW.DayOfWeek = "Вторник";
                            break;
                        case "ср":
                            DW.DayOfWeek = "Среда";
                            break;
                        case "чт":
                            DW.DayOfWeek = "Четверг";
                            break;
                        case "пт":
                            DW.DayOfWeek = "Пятница";
                            break;
                        case "сб":
                            DW.DayOfWeek = "Суббота";
                            break;
                        case "вс":
                            DW.DayOfWeek = "Воскресенье";
                            break;
                        default:
                            DW.DayOfWeek = "Завтра";
                            break;
                    }

                    Content = Content.Substring(Content.IndexOf(Value1));
                    End = Content.IndexOf(ValueDiv);
                    SubStr = Content.Substring(Start1, End - Start1);
                    string tomorrow = "<a name=\"tomorrow\">";
                    int m = SubStr.IndexOf(tomorrow);
                    if (m != -1)
                    {
                        End = SubStr.IndexOf("</a>");
                        int Start09 = tomorrow.Length;
                        SubStr = SubStr.Substring(Start09, End - Start09);
                    }
                    Content = Content.Substring(Content.IndexOf(Value15));
                    End = Content.IndexOf(ValueDiv);
                    SubStr += " " + Content.Substring(Start15, End - Start15);
                    DW.Date = SubStr;

                    Content = Content.Substring(Content.IndexOf(Value2));
                    Content = Content.Substring(Content.IndexOf(Value25));
                    End = Content.IndexOf(ValueDiv);
                    SubStr = Content.Substring(Start25, End - Start25);
                    SubStr = SubStr.Replace("…", "...");
                    DW.Temperature = SubStr;

                    Content = Content.Substring(Content.IndexOf(Value3));
                    End = Content.IndexOf(ValueDiv);
                    Content = Content.Substring(End);
                    Content = Content.Substring(Content.IndexOf(Value3));
                    End = Content.IndexOf(ValueDiv);
                    SubStr = Content.Substring(Start3, End - Start3);
                    DW.WeatherKind = SubStr;

                    Content = Content.Substring(End);
                    Content = Content.Substring(Content.IndexOf(Value3));
                    End = Content.IndexOf(ValueDiv);
                    SubStr = Content.Substring(Start3, End - Start3);
                    DW.Pressure = SubStr;

                    Content = Content.Substring(End);
                    Content = Content.Substring(Content.IndexOf(Value3));
                    End = Content.IndexOf(ValueDiv);
                    SubStr = Content.Substring(Start3, End - Start3);
                    DW.Humidity = SubStr;

                    //WeatherList.Add(DW);
                    TreeViewItem TN = new TreeViewItem();
                    TN.Header = DW.Date;
                    TN.Tag = DW;
                    DatesTreeView.Items.Add(TN);
                }

                //foreach (var DW in WeatherList)
                //{
                //    TreeViewItem TN = new TreeViewItem();
                //    TN.Header = DW.Date;
                //    TN.Tag = DW;
                //    DatesTreeView.Items.Add(TN);
                //}
            }
            catch
            {
                //MessageBox.Show("Возможно, не подключен интернет", "Возникла ошибка");
            }

        }
    }

    public class DayWeather
    {
        public string Temperature { get; set; }
        public string Date { get; set; }
        public string DayOfWeek { get; set; }
        public string WeatherKind { get; set; }
        public string Pressure { get; set; }
        public string Humidity { get; set; }
    }
}
