#include "identifier.h"

identifier::identifier()
{
}

char *identifier::getString(char *string)
{
    int i = 0;
    int inc = 1;
    while (string[i])
        i++;

    if (i == 0)
        return string;

    int length = i + 1;
    i--;
    while (i && inc) {
        if (string[i] == '9')
            string[i] = '1';
        else {
            string[i]++;
            inc = 0;
            return string;
        }
        i--;
        bool ch;
        do {
            switch (string[i]) {
                case 'D':
                case 'F':
                case 'G':
                case 'J':
                case 'M':
                case 'Q':
                case 'V':
                    ch = true;
                    string[i] += inc + 1;
                    inc = 0;
                    break;
                case 'Z':
                    if (inc) {
                        string[i] = 'A';
                        if (i && string[--i] == '-') {
                            i--;
                            ch = false;
                        }
                        else {
                            char *nString = new char[length+3];
                            nString[0] = 'A';
                            nString[1] = '1';
                            nString[2] = '-';
                            nString[length+2] = 0;
                            for (int j = 3; j <= length; j++)
                                nString[j] = string[j-3];

                            delete string;
                            string = nString;
                            ch = false;
                            inc = 0;
                        }
                    }
                    else
                        ch = false;
                    break;
                 default:
                 	if (inc == 0) {
                     	ch = false;
                     	break;
                 	}
                    string[i] += inc;
                    ch = true;
                    inc = 0;
                    break;
            }
        }while(ch);
    }
}
