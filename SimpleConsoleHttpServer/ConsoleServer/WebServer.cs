﻿namespace ConsoleServer {
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SQLite;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Xml.Linq;

    /// <summary>
    /// Способ хранения информации
    /// </summary>
    public enum StorageType {
        /// <summary>
        /// В XML-файле
        /// </summary>
        XML,
        /// <summary>
        /// В SQLite-базе
        /// </summary>
        SQLite
    }

    /// <summary>
    /// Веб-сервер
    /// </summary>
    public sealed class WebServer {
        private HttpListener listener;
        /// <summary>
        /// Папка для веб-страниц
        /// </summary>
        private string baseFolder;

        /// <summary>
        /// Путь к базе данных
        /// </summary>
        private string DBPath;

        /// <summary>
        /// Способ хранения 
        /// </summary>
        private StorageType storage;

        private string Host = string.Empty;

        /// <summary>
        /// Конструктор WebServer
        /// </summary>
        /// <param name="URIPrefix">Адрес сервера</param>
        /// <param name="BaseFolder">Папка для хранения страниц</param>
        public WebServer(string URIPrefix, string BaseFolder, StorageType Storage, string DBPath) {
            if (!Directory.Exists(BaseFolder))
                throw new ArgumentException();

            if (URIPrefix == string.Empty)
                throw new ArgumentException();

            if (File.Exists(DBPath))
                this.DBPath = DBPath;
            else if (DBPath == string.Empty)
                this.DBPath = (Storage == StorageType.XML) ? "Guestbook.s3db" : "Guestbook.xml";
            else
                throw new ArgumentException();

            this.baseFolder = BaseFolder;
            this.storage = Storage;
            this.listener = new HttpListener();
            this.listener.Prefixes.Add(URIPrefix);
            this.listener.Prefixes.Add("http://localhost:800/");

            ThreadPool.SetMaxThreads(50, 1000);
            ThreadPool.SetMinThreads(50, 50);
        }

        /// <summary>
        /// Запуск сервера (желательно, в отдельном потоке)
        /// </summary>
        public void Start() // Run this on a separate thread, as
        { // we did before.
            this.listener.Start();
            while (true)
                try {
                    HttpListenerContext request = listener.GetContext();
                    ThreadPool.QueueUserWorkItem(ProcessRequest, request);
                }
                catch (HttpListenerException) { break; } // Listener stopped.
                catch (InvalidOperationException) { break; } // Listener stopped.
        }

        /// <summary>
        /// Остановка сервера
        /// </summary>
        public void Stop() { listener.Stop(); }

        /// <summary>
        /// Журналирование запросов
        /// </summary>
        /// <param name="LogText">Текст для вывода</param>
        private void Logging(string LogText) {
            Console.WriteLine(string.Format("Now: {0}\n\t{1}", DateTime.Now.ToString(), LogText));
        }

        private byte[] GetWebPage(string uri) {
            WebClient client = new WebClient();
            client.Headers.Add("UserAgent", "Mozilla/5.0 (UnixXXX; U; Windows NT 5.3;) Firefox/6.0.6.0");
            string content = client.DownloadString(uri);
            return Encoding.Default.GetBytes(content);
        }
        /// <summary>
        /// Обработка запросов
        /// </summary>
        /// <param name="ListenerContext">Контекст</param>
        private void ProcessRequest(object ListenerContext) {
            try {
                var context = (HttpListenerContext)ListenerContext;
                string filename = Path.GetFileName(context.Request.RawUrl);
                string directoryname = Path.GetDirectoryName(context.Request.RawUrl);
                string path = (directoryname != null && directoryname.Length > 1) ? Path.Combine(this.baseFolder, directoryname.Substring(1)) : this.baseFolder;
                path = Path.Combine(path, filename);
                byte[] msg = null;
                if (!File.Exists(path)) {
                    if (context.Request.RawUrl.StartsWith("/proxy/")) {
                        string uri = context.Request.RawUrl;
                        string site = uri.Substring(7);
                        string httpsite = string.Format("http://{0}", site);
                        if (site != string.Empty) {
                            this.Logging(string.Format("Proxy: страница сайта \"{0}\" обрабатывается", site));
                            msg = GetWebPage(httpsite);

                            Uri siteURI;
                            Host = (Uri.TryCreate(httpsite, UriKind.RelativeOrAbsolute, out siteURI)) ? siteURI.Host : string.Empty;
                        }
                    }
                    else if (context.Request.RawUrl == "/guestbook/") {
                        switch (context.Request.HttpMethod) {
                            case "GET":
                                this.Logging(string.Format("Guestbook: выгрузка сообщений из {0}", storage.ToString()));
                                StringBuilder xmldoc = new StringBuilder();
                                xmldoc.Append("<table><tr><th>Date</th><th>User</th><th>Message</th></tr>");
                                switch (storage) {
                                    case StorageType.XML:
                                        lock (this.DBPath) {
                                            XElement xe = XElement.Load(DBPath);
                                            foreach (var xelem in xe.Elements())
                                                xmldoc.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>",
                                                    xelem.Attribute("date"), xelem.Attribute("user"), xelem.Attribute("message"));
                                        }
                                        break;
                                    case StorageType.SQLite:
                                        lock (this.DBPath) {
                                            XElement xel = new XElement("messages");
                                            using (SQLiteConnection connection = new SQLiteConnection()) {
                                                connection.ConnectionString = string.Format("Data Source = {0}", DBPath);
                                                if (connection.State != ConnectionState.Open)
                                                    connection.Open();

                                                using (SQLiteCommand command = new SQLiteCommand(connection)) {
                                                    command.CommandType = CommandType.Text;
                                                    command.CommandText = @"SELECT m.date, u.name, m.message FROM Users u, Messages m WHERE u.id=m.userID;";
                                                    using (SQLiteDataReader dr = command.ExecuteReader()) {
                                                        while (dr.Read()) {
                                                            xmldoc.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>",
                                                                DateTime.Parse(Convert.ToString(dr[0])), Encoding.UTF8.GetString((byte[])dr[1]), Encoding.UTF8.GetString((byte[])dr[2]));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        throw new Exception();
                                }
                                xmldoc.Append("</table>");
                                string response = "<html><head><title>Guest Book</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><style type=\"text/css\">TABLE,TD { border:1px solid black; text-align:center; border-collapse: collapse; } </style></head><body><p align=\"center\">{0}</p></body></html>".Replace("{0}", xmldoc.ToString());
                                msg = Encoding.UTF8.GetBytes(response);
                                break;
                            case "POST":
                                this.Logging(string.Format("Guestbook: загрузка сообщения в {0}", storage.ToString()));
                                Stream requestStream = context.Request.InputStream;
                                List<byte> postContent = new List<byte>();
                                for (int tempChar;
                                    (tempChar = requestStream.ReadByte()) != -1;
                                    postContent.Add((byte)tempChar))
                                    ;
                                string[] postVariables = Encoding.UTF8.GetString(postContent.ToArray()).Split('&');
                                Dictionary<string, string> postTable = new Dictionary<string, string>();
                                foreach (var str in postVariables) {
                                    string[] strHT = str.Split('=');
                                    postTable.Add(strHT[0], strHT[1]);
                                }
                                Message Message = new Message() { Date = DateTime.Parse(postTable["date"]), Mess = postTable["message"], User = postTable["user"] };
                                switch (storage) {
                                    case StorageType.XML:
                                        lock (DBPath) {
                                            XElement element = Message.ToXElement();
                                            XElement xe = XElement.Load(DBPath);
                                            xe.Add(element);
                                            xe.Save(DBPath);
                                        }
                                        break;
                                    case StorageType.SQLite:
                                        lock (DBPath) {
                                            using (SQLiteConnection connection = new SQLiteConnection()) {
                                                connection.ConnectionString = string.Format("Data Source = {0}", DBPath);
                                                if (connection.State != ConnectionState.Open)
                                                    connection.Open();

                                                using (SQLiteCommand command = new SQLiteCommand(connection)) {
                                                    command.CommandType = CommandType.Text;
                                                    command.CommandText = string.Format(@"SELECT id FROM Users WHERE name='{0}'", Message.User);
                                                    object obj = command.ExecuteScalar();
                                                    int userID = (obj is DBNull) ? 0 : Convert.ToInt32(obj);
                                                    if (userID == 0) {
                                                        command.CommandText = string.Format(@"INSERT INTO Users (name) VALUES ('{0}')", Message.User);
                                                        command.ExecuteNonQuery();
                                                        command.CommandText = @"SELECT MAX(id) FROM Users";
                                                        obj = command.ExecuteScalar();
                                                        userID = Convert.ToInt32(obj);
                                                    }
                                                    command.CommandText = string.Format(@"INSERT INTO Messages (userID, message, date) VALUES ({0}, '{1}', '{2}')", userID, Message.Mess, Message.Date.ToString());
                                                    command.ExecuteNonQuery();
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        throw new Exception();
                                }
                                break;
                        }
                    }
                    else {
                        if (Host != string.Empty) {
                            string uri = context.Request.RawUrl;                            
                            Uri siteURI;
                            string site = string.Format("http://{0}/{1}", Host, uri);
                            if (Uri.TryCreate(site, UriKind.RelativeOrAbsolute, out siteURI)) {
                                try {
                                    msg = GetWebPage(site);
                                }
                                catch { }
                            }
                        }
                        else if (File.Exists(Path.Combine(this.baseFolder, "index.htm")))
                            msg = File.ReadAllBytes(Path.Combine(this.baseFolder, "index.htm"));

                    }
                }
                else
                    msg = File.ReadAllBytes(path);

                context.Response.StatusCode = (int)HttpStatusCode.OK;
                if (msg == null)
                    msg = Encoding.UTF8.GetBytes("<html><head><title>Main page</title></head><body><h1 style=\"text-align:center; vertical-align: baseline ;\">Hello World!</h1></body></html>");

                context.Response.ContentLength64 = msg.Length;
                using (Stream s = context.Response.OutputStream)
                    s.Write(msg, 0, msg.Length);
            }
            catch (Exception ex) { Console.WriteLine("Request error: {0}", ex); }
        }
    }
}
