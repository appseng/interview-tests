﻿namespace ConsoleServer {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Xml.Linq;

    /// <summary>
    /// Запись в книге
    /// </summary>
    public class Message {
        public DateTime Date { set; get; }

        public string User { set; get; }

        public string Mess { set; get; }

        public override string ToString() {
            return string.Format("<date>{0}</date>\n<user>{1}</user>\n<message>{2}</message>", Date, User, Mess);
        }

        public XElement ToXElement() {
            return new XElement("Message", new XAttribute("date", Date.ToString()), new XAttribute("user", User), new XAttribute("message", Mess));
        }
    }
}
