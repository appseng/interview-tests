﻿namespace ConsoleServer {
    using System;
    using System.Collections.Specialized;
    using System.Configuration;

    class Program {
        static void Main() {
            NameValueCollection appSettings = ConfigurationManager.AppSettings;
            StorageType storage = (StorageType)Enum.Parse(typeof(StorageType), appSettings["StorageType"]);
            string dbXMLFile = Convert.ToString(appSettings["XMLDB"]);
            string dbSQLite = Convert.ToString(appSettings["SQLiteDB"]);
            string baseFolder = Convert.ToString(appSettings["BaseFolder"]);
            string uriPrefix = Convert.ToString(appSettings["URIPrefix"]);
            if (dbXMLFile == string.Empty)
                dbXMLFile = "Guestbook.xml";
            if (dbSQLite == string.Empty)
                dbSQLite = "Guestbook.s3db";
            if (baseFolder == string.Empty)
                baseFolder = ".";
            if (uriPrefix == string.Empty)
                uriPrefix = "http://127.0.0.1/";

            string DBPath = (storage == StorageType.XML) ? dbXMLFile : dbSQLite;
            // Listen on the default port (80), serving files in e:\mydocs\webroot:
            var server = new WebServer(uriPrefix, baseFolder, storage, DBPath);
            // Start the server on a parallel thread:
            new System.Threading.Thread(server.Start).Start();
            Console.WriteLine("Server running... press Enter to stop");
            Console.ReadLine();
            server.Stop();
        }
    }
}
