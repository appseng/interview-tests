﻿namespace IncrementID {
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Приращение идентификатора
    /// </summary>
    public static class IncrementID {
        private static string incrementIdentifier = string.Empty;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public static string IncrementIdentifier {
            get {
                lock (incrementIdentifier) {
                    return incrementIdentifier;
                }
            }
            set {
                lock (incrementIdentifier) {
                    incrementIdentifier = value;
                }
            }
        }

        /// <summary>
        /// Возвращает идентификатор после приращения
        /// </summary>
        /// <param name="Increment">Приращение</param>
        /// <returns></returns>
        public static string Increment(string Increment) {
            lock (incrementIdentifier) {
                for (int j = 0; j < Increment.Length; j++) {
                    char tp = Increment[j];
                    if (tp >= '0' && tp <= '9')
                        continue;

                    incrementIdentifier += Increment;
                    return incrementIdentifier;
                }

                int lastIndex = IncrementIdentifier.Length;
                for (int i = incrementIdentifier.Length - 1; i > 0; i--) {
                    char tp = incrementIdentifier[i];
                    if (tp >= '0' && tp <= '9')
                        continue;

                    lastIndex = i + 1;
                    break;
                }
                if (incrementIdentifier.Length == lastIndex) {
                    incrementIdentifier += Increment;
                    return IncrementIdentifier;
                }
                else {
                    string numericString = incrementIdentifier.Substring(lastIndex);
                    string ownString = incrementIdentifier.Substring(0, lastIndex);

                    int maxLength = (numericString.Length > Increment.Length) ? numericString.Length : Increment.Length;
                    int[] numFirst = numericString.ConvertToInts(maxLength);
                    int[] numIncrement = Increment.ConvertToInts(maxLength);

                    int overLoadValue = 0;

                    for (int i = 0; i < numFirst.Length; i++) {
                        int nextNumber = numFirst[i] + numIncrement[i] + overLoadValue;
                        if (nextNumber > 9) {
                            overLoadValue = 1;
                            nextNumber -= 10;
                        }
                        else if (nextNumber < 0) {
                            overLoadValue = -1;
                            nextNumber += 10;
                        }
                        else
                            overLoadValue = 0;

                        numFirst[i] = nextNumber;
                    }

                    string advString = (overLoadValue != 0) ? overLoadValue.ToString() : "";
                    incrementIdentifier = ownString + advString + numFirst.IntsToString();
                    return incrementIdentifier;
                }
            }
        }

        //Вспомогательные функции
        private static int[] ConvertToInts(this string BaseString, int Length) {
            List<int> ints = new List<int>();
            for (int i = BaseString.Length - 1; i >= 0; i--) {
                ints.Add(Convert.ToInt32(BaseString[i]) - 48);
            }
            for (int i = BaseString.Length; i < Length; i++)
                ints.Add(0);

            return ints.ToArray();
        }
        private static string IntsToString(this int[] Ints) {
            StringBuilder sb = new StringBuilder();
            for (int i = Ints.Length - 1; i >= 0; i--) {
                sb.Append(Ints[i]);
            }
            return sb.ToString();
        }
    }
}