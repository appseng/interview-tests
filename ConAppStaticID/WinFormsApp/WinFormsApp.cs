﻿using System;
using System.Windows.Forms;

namespace WinFormsApp
{
    public partial class IncrementIdentifierForm : Form
    {
        public IncrementIdentifierForm()
        {
            InitializeComponent();
            IncrementID.IncrementID.IncrementIdentifier = OutputValue.Text = Identifier.Text;
        }

        private void equals_Click(object sender, EventArgs e)
        {
            OutputValue.Text = Identifier.Text = IncrementID.IncrementID.Increment(Increment.Text);
        }
    }
}
