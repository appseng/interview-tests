﻿using System;
using System.Windows.Forms;
using Visio = Microsoft.Office.Interop.Visio;

namespace DmitryVisioAddIn
{
    public partial class ThisAddIn
    {
        private int nID = -1;
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.ShapeAdded += Application_ShapeAdded;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
               this.Application.ShapeAdded -= Application_ShapeAdded;
        }

        private void Application_ShapeAdded(Visio.Shape curShape)
        {
            double curbeginX;
            double curbeginY;
            double curendX;
            double curendY;
            //int nID = -1;

            try{
                Visio.Connects curToConns = curShape.Connects;
                Visio.Connects curFromConns = curShape.FromConnects;
                if (curToConns.Count != 0 || curFromConns.Count != 0)
                    return;

                Visio.Shape oldShape = null;
                curbeginX = curShape.Cells["PinX"].Result[Visio.VisUnitCodes.visMillimeters] -
                    curShape.Cells["Width"].Result[Visio.VisUnitCodes.visMillimeters] / 2;

                curbeginY = curShape.Cells["PinY"].Result[Visio.VisUnitCodes.visMillimeters] -
                    curShape.Cells["Height"].Result[Visio.VisUnitCodes.visMillimeters] / 2;

                curendX = curShape.Cells["PinX"].Result[Visio.VisUnitCodes.visMillimeters] +
                    curShape.Cells["Width"].Result[Visio.VisUnitCodes.visMillimeters] / 2;

                curendY = curShape.Cells["PinY"].Result[Visio.VisUnitCodes.visMillimeters] +
                    curShape.Cells["Height"].Result[Visio.VisUnitCodes.visMillimeters] / 2;

                foreach (var shp in this.Application.ActivePage.Shapes)
                {
                    Visio.Shape Shape = (Visio.Shape)shp;

                    double beginX;
                    double beginY;
                    double endX;
                    double endY;
                    beginX = Shape.Cells["PinX"].Result[Visio.VisUnitCodes.visMillimeters] -
                        Shape.Cells["Width"].Result[Visio.VisUnitCodes.visMillimeters] / 2;

                    beginY = Shape.Cells["PinY"].Result[Visio.VisUnitCodes.visMillimeters] -
                        Shape.Cells["Height"].Result[Visio.VisUnitCodes.visMillimeters] / 2;

                    endX = Shape.Cells["PinX"].Result[Visio.VisUnitCodes.visMillimeters] +
                        Shape.Cells["Width"].Result[Visio.VisUnitCodes.visMillimeters] / 2;

                    endY = Shape.Cells["PinY"].Result[Visio.VisUnitCodes.visMillimeters] +
                        Shape.Cells["Height"].Result[Visio.VisUnitCodes.visMillimeters] / 2;


                    bool crossedShaped =
                        curbeginX >= beginX && curbeginX <= endX && curbeginY >= beginY && curbeginY <= endY ||
                        //curbeginX <= beginX && curbeginX >= endX && curbeginY <= beginY && curbeginY >= endY ||
                        curendX <= endX && curendX >= beginX && curbeginY >= beginY && curbeginY <= endY ||
                        //curendX >= endX && curendX <= beginX && curbeginY <= beginY && curbeginY >= endY ||
                        curendX <= endX && curendX >= beginX && curendY <= endY && curendY >= beginY ||
                        //curendX >= endX && curendX <= beginX && curendY >= endY && curendY <= beginY ||
                        curbeginX >= beginX && curbeginX <= endX && curendY <= endY && curendY >= beginY;// ||
                        //curbeginX <= beginX && curbeginX >= endX && curendY >= endY && curendY <= beginY;

                    if (crossedShaped && !curShape.Equals(Shape) && curShape.get_AreaIU() != 0)
                    {
                        oldShape = Shape;
                        break;
                    }
                }
                if (oldShape == null)
                    return;

                nID = Application.BeginUndoScope("Addon");

                if (MessageBox.Show("Should the shape be replaced by a new empty shape?", "Replacement", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Visio.Connects toConns = oldShape.Connects;
                    Visio.Connects fromConns = oldShape.FromConnects;

                    //Array toConns = oldShape.ConnectedShapes(Visio.VisConnectedShapesFlags.visConnectedShapesIncomingNodes, "");
                    //Array fromConns = oldShape.ConnectedShapes(Visio.VisConnectedShapesFlags.visConnectedShapesOutgoingNodes, "");

                    //Visio.Master ms = Shape.Master;
                    //oldShape.ReplaceShape(ms);
                    foreach (var item in toConns)
                    {
                        ((Visio.Connect)item).ToCell.GlueTo(curShape.CellsU["PinX"]);
                    }
                    foreach (var item in fromConns)
                    {
                        ((Visio.Connect)item).FromCell.GlueTo(curShape.CellsU["PinX"]);
                    }
                    oldShape.DeleteEx((int)Visio.VisDeleteFlags.visDeleteNoHealConnectors);
                }
                else
                    curShape.Delete();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
            }
            finally
            {
                    Application.EndUndoScope(nID, true); 
            }
        }

        #region Код, автоматически созданный VSTO

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
