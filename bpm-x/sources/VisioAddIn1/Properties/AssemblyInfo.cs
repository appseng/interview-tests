﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// Управление общими сведениями о сборке осуществляется с помощью 
// набора атрибутов. Отредактируйте эти значения атрибутов, чтобы изменить сведения,
// сопоставленные со сборкой.
[assembly: AssemblyTitle("DmitryVisioAddIn")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Dmitry")]
[assembly: AssemblyProduct("DmitryVisioAddIn")]
[assembly: AssemblyCopyright("Copyright © Dmitry 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения False в параметре ComVisible делает типы в этой сборке невидимыми 
// для компонентов COM.  Если необходимо обратиться к типу в этой сборке через 
// COM, следует установить атрибут ComVisible в TRUE для этого типа.
[assembly: ComVisible(true)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект видим для COM
[assembly: Guid("893f51ad-2093-4242-bd1e-0c7ae1d6392d")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер построения
//      Версия
//
// Можно задать все значения или принять номер построения и номер редакции по умолчанию 
// с помощью знака '*', как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.1.0")]
[assembly: AssemblyFileVersion("0.0.1.0")]

