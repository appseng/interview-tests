program autorun;

{$APPTYPE CONSOLE}

uses
  Windows,
  Registry,
  ShellApi,
  SysUtils;

function NetFramework35: integer;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  Reg.OpenKey('SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5', False);
  IF Reg.ValueExists('Install') then
    Result := Reg.ReadInteger('Install')
  else
    Result := 0;
  Reg.CloseKey;
  Reg.Free;
end;

function NetFramework40Full: integer;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  Reg.OpenKey('SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full', False);
  IF Reg.ValueExists('Install') then
    Result := Reg.ReadInteger('Install')
  else
    Result := 0;
  Reg.CloseKey;
  Reg.Free;
end;

function NetFramework40Client: integer;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  Reg.OpenKey('SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client', False);
  IF Reg.ValueExists('Install') then
    Result := Reg.ReadInteger('Install')
  else
    Result := 0;
  Reg.CloseKey;
  Reg.Free;
end;

function NetFramework45: integer;
var
  Reg: TRegistry;
begin
Reg := TRegistry.Create;
Reg.RootKey := HKEY_LOCAL_MACHINE;
Reg.OpenKey('SOFTWARE\Microsoft\NET Framework Setup\NDP\v4.5', False);
IF Reg.ValueExists('Install') then
  Result := Reg.ReadInteger('Install')
else
  Result := 0;
Reg.CloseKey;
Reg.Free;
end;

function NetFramework50: integer;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  Reg.OpenKey('SOFTWARE\Microsoft\NET Framework Setup\NDP\v5.0', False);
  IF Reg.ValueExists('Install') then
    Result := Reg.ReadInteger('Install')
  else
    Result := 0;
  Reg.CloseKey;
  Reg.Free;
end;

function Silverlight: string;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  Reg.OpenKey('SOFTWARE\Microsoft\Silverlight', False);
  IF Reg.ValueExists('Version') then
    Result := Reg.ReadString('Version')
  else
    Result := '';
  Reg.CloseKey;
  Reg.Free;
end;

var
  F:              text;
  buffer:         string;

  install35:      integer;
  install40Full:  integer;
  install40Client: integer;
  install45:      integer;
  install50:      integer;
  slvrlight:      string;
  netdetection:   string;
begin
  { TODO -oUser -cConsole Main : Insert code here }
  
  { �������� ������� ������, ������� ������� ���������� ��� ����������������}
  if FileExists('filelist.txt') then
    begin
    AssignFile(F, 'filelist.txt');
    Reset(F);
    while (not EOF(F)) do
      begin
      ReadLn(F, buffer);
      if not FileExists(buffer) then
        begin
        Write('File ', buffer);
        WriteLn(' doesn''t exist.');
        end
      else
        begin
        Write('File ', buffer);
        WriteLn(' exists.');
        end;;
      end;
    Close(F);
    end
  else
    WriteLn('File ''filelist.txt'' doesn''t exist');

  { ��������� ���������� � ������� ������������� ����������� .NET }
  install35 := NetFramework35;
  install40Full := NetFramework40Full;
  install40Client := NetFramework40Client;
  install45 := NetFramework45;
  install50 := NetFramework50;
  slvrlight := Silverlight;

  { �������� ������� ������ ���������� ��� ������ ������ }
  netdetection := '';
  if install50 = 1 then
    netdetection := '5.0'
  else if install45 = 1 then
    netdetection := '4.5'
  else if install40Client = 1 then
    netdetection := '4.0Client'
  else if install40Full = 1 then
    netdetection := '4.0Full'
  else if install35 = 1 then
    netdetection := '3.5';  

  if netdetection = '' then { � ������� .Net Framework �� ���������� }
    begin
    WriteLn('.Net Framework 3.5 or higher hasn''t been installed. ');
    ShellExecute(0, 'open', 'nonet.html', nil, nil, SW_SHOWNORMAL);
    end
  else { � ������� .Net Framework ���������� }
    begin
    Write('.Net Framework ', netdetection);
    WriteLN(' has been installed.');
    end;

  if (slvrlight <> '') and (AnsiCompareStr(slvrlight, '5.0.0.0') >= 0) then
    begin  { � ������� SilverLight ���������� }
    Write('SilverLight ', slvrlight);
    WriteLn(' has been installed.');
    end
  else
    begin { � ������� SilverLight �� ���������� }
    WriteLn('SilverLight 5.0 or higher hasn''t been installed. ');
    ShellExecute(0, 'open', 'nosl.html', nil, nil, SW_SHOWNORMAL);
    end;

  ReadLn;
end.
