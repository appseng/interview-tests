﻿#region Lisence
/*
 * Copyright (c) 2015, Dmitry Kuznetsov
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion

namespace Internet.Store
{
    using System.Collections.Generic;
    using System.Text;

    #region Enums
    /// <summary>
    /// Type of disk
    /// </summary>
    public enum DiskType
    {
        CD,
        DVD,
        Other
    }

    /// <summary>
    /// Type of content
    /// </summary>
    public enum ContentType
    {
        Music,
        Video,
        Software,
        Other
    } 
    #endregion

    #region Abstract classes
    /// <summary>
    /// Common goods
    /// </summary>
    public abstract class Goods
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string BarCode { get; set; }

        public override string ToString()
        {
            return string.Format("  {0} - {1}\n    Price:{2}\n    A bar code:{3}\n", this.GetType().Name.ToString(), Title, Price, BarCode);
        }
    }    
    
    /// <summary>
    /// Common book
    /// </summary>
    public abstract class Book : Goods
    {
        public int PagesCount { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("    A pages count:{0}\n", PagesCount);
        }

    }
    #endregion

    /// <summary>
    /// Disk class
    /// </summary>
    public class Disk : Goods
    {
        public DiskType Type { get; set; }
        public ContentType Content { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("    A type:{0}\n    Constent:{1}", Type, Content);
        }
    }

    #region Different books
    /// <summary>
    /// Book about programming
    /// </summary>
    public class ProgrammingBook : Book
    {
        public string Language { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("    A language:{0}", Language);
        }
    }

    /// <summary>
    /// Book about cooking
    /// </summary>
    public class CookBook : Book
    {
        public string MainIngredient { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("    A main ingredient:{0}", MainIngredient);
        }
    }

    /// <summary>
    /// Book about esoterism
    /// </summary>
    public class EsotericismBook : Book
    {
        public int MinReadersAge { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("    Minimum readers' age :{0}", MinReadersAge);
        }
    }
    #endregion

    /// <summary>
    /// Warehouse class
    /// </summary>
    public class WareHouse
    {
        public List<Goods> Goods { get; set; }

        public override string ToString()
        {
            if (Goods == null || Goods != null && Goods.Count == 0)
                return "The warehouse has no goods.";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("The Internet Store has goods in the warehouse:");
            foreach (var good in Goods)
                sb.AppendLine(good.ToString());

            return sb.ToString();
        }
    }
    
    /// <summary>
    /// Internet store shop
    /// </summary>
    public class InternetStore
    {
    	public WareHouse WH { get; set;}
    	
    	public InternetStore(List<Goods> g)
    	{
    		WH = new WareHouse();
    		WH.Goods = g;
    	}
    }
}
