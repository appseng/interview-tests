﻿#region Lisence
/*
 * Copyright (c) 2015, Dmitry Kuznetsov
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion

namespace Internet.Store
{
    using System;
    using System.Collections.Generic;

    class Program
    {
        static void Main(string[] args)
        {
            List<Goods> Goods = new List<Goods>() {
                new ProgrammingBook() { Title="Алгоритмы и структуры данных", Language="Pascal", Price=19.99m, PagesCount=352, BarCode="9785794000658"},
                new CookBook() { Title="Большая поваренная книга", MainIngredient="Мясо", Price=12.99m, PagesCount=257, BarCode="9785699132133"},
                new EsotericismBook() { Title="Эзотерика. Парапсихология", MinReadersAge=6, Price= 15.99m, PagesCount=674, BarCode="978588928041X"},

                new Disk() { Title="Twin Peaks, Pilot + alternative end (europilot) Disk 1", Content= ContentType.Video, Type= DiskType.DVD, Price=20.99m, BarCode="123123123123X"},
                new Disk() { Title=@"Струп / Диск музыки. Альбом для рисования", Content= ContentType.Music, Type= DiskType.CD, Price=9.99m, BarCode="321321312321X"},
                new Disk() { Title="Sql Server 2012 Developer x86-x64", Content= ContentType.Software, Type= DiskType.DVD, Price=12.00m, BarCode="1234567890123"}
            };

            // creating an instance of the internetstore class
            InternetStore store = new InternetStore(Goods);

            // output a list of goods in the warestore of the internetstore
            Console.WriteLine(store.WH);
            
            // waiting for pressing ENTER button
            Console.Write("Press ENTER to exit...");
            Console.ReadLine();
        }
    }
}
