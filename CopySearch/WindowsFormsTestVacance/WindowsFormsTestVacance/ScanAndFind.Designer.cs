﻿namespace WindowsFormsTestVacance {
    partial class ScanAndFind {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent() {
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.DuplicateFile = new System.Windows.Forms.ListView();
            this.Name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Folder = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Size = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LastWriteTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Start = new System.Windows.Forms.Button();
            this.CurrentFile = new System.Windows.Forms.Label();
            this.Pause = new System.Windows.Forms.Button();
            this.Stop = new System.Windows.Forms.Button();
            this.FoundScan = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Progress
            // 
            this.Progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Progress.Location = new System.Drawing.Point(12, 266);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(508, 23);
            this.Progress.Step = 1;
            this.Progress.TabIndex = 0;
            // 
            // DuplicateFile
            // 
            this.DuplicateFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DuplicateFile.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Name,
            this.Folder,
            this.Size,
            this.LastWriteTime,
            this.Type});
            this.DuplicateFile.Location = new System.Drawing.Point(12, 33);
            this.DuplicateFile.Name = "DuplicateFile";
            this.DuplicateFile.Size = new System.Drawing.Size(508, 196);
            this.DuplicateFile.TabIndex = 1;
            this.DuplicateFile.UseCompatibleStateImageBehavior = false;
            this.DuplicateFile.View = System.Windows.Forms.View.Details;
            // 
            // Name
            // 
            this.Name.Text = "Имя";
            this.Name.Width = 74;
            // 
            // Folder
            // 
            this.Folder.Text = "Папка";
            this.Folder.Width = 122;
            // 
            // Size
            // 
            this.Size.Text = "Размер";
            this.Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LastWriteTime
            // 
            this.LastWriteTime.Text = "Дата изменения";
            this.LastWriteTime.Width = 105;
            // 
            // Type
            // 
            this.Type.Text = "Тип файла";
            this.Type.Width = 115;
            // 
            // Start
            // 
            this.Start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Start.Location = new System.Drawing.Point(12, 303);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(75, 23);
            this.Start.TabIndex = 2;
            this.Start.Text = "Старт";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // CurrentFile
            // 
            this.CurrentFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CurrentFile.AutoSize = true;
            this.CurrentFile.Location = new System.Drawing.Point(12, 241);
            this.CurrentFile.Name = "CurrentFile";
            this.CurrentFile.Size = new System.Drawing.Size(0, 13);
            this.CurrentFile.TabIndex = 3;
            // 
            // Pause
            // 
            this.Pause.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Pause.Location = new System.Drawing.Point(228, 303);
            this.Pause.Name = "Pause";
            this.Pause.Size = new System.Drawing.Size(75, 23);
            this.Pause.TabIndex = 4;
            this.Pause.Text = "Пауза";
            this.Pause.UseVisualStyleBackColor = true;
            this.Pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // Stop
            // 
            this.Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Stop.Location = new System.Drawing.Point(445, 303);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(75, 23);
            this.Stop.TabIndex = 5;
            this.Stop.Text = "Стоп";
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.Stop_Click);
            // 
            // FoundScan
            // 
            this.FoundScan.AutoSize = true;
            this.FoundScan.Location = new System.Drawing.Point(12, 9);
            this.FoundScan.Name = "FoundScan";
            this.FoundScan.Size = new System.Drawing.Size(341, 13);
            this.FoundScan.TabIndex = 6;
            this.FoundScan.Text = "Найдено одинаковых файлов 0. Всего просканировано файлов 0.";
            // 
            // ScanAndFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 338);
            this.Controls.Add(this.FoundScan);
            this.Controls.Add(this.Stop);
            this.Controls.Add(this.Pause);
            this.Controls.Add(this.CurrentFile);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.DuplicateFile);
            this.Controls.Add(this.Progress);
            this.MinimumSize = new System.Drawing.Size(540, 372);
            this.Text = "Поиск дубликатов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar Progress;
        private System.Windows.Forms.ListView DuplicateFile;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Label CurrentFile;
        private System.Windows.Forms.Button Pause;
        private System.Windows.Forms.Button Stop;
        private System.Windows.Forms.Label FoundScan;
        private System.Windows.Forms.ColumnHeader Name;
        private System.Windows.Forms.ColumnHeader Folder;
        private System.Windows.Forms.ColumnHeader Size;
        private System.Windows.Forms.ColumnHeader LastWriteTime;
        private System.Windows.Forms.ColumnHeader Type;
    }
}

