﻿namespace WindowsFormsTestVacance {
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;
    using System;

    public class Work {
        /// <summary>
        /// Label-control of processed filename
        /// </summary>
        public Label currentFile { get; set; }

        /// <summary>
        /// Control of progress-line
        /// </summary>
        public ProgressBar progress { get; set; }

        /// <summary>
        /// Control of files list
        /// </summary>
        public ListView dublicates { get; set; }

        /// <summary>
        /// Scanned and found files
        /// </summary>
        public Label foundScan { get; set; }

        /// <summary>
        /// Count of dublicated files
        /// </summary>
        private int dublicatesCount;

        /// <summary>
        /// Count of all files
        /// </summary>
        private int filesCount;

        /// <summary>
        /// Informational list
        /// </summary>
        private List<FileInfo> fileInfo = new List<FileInfo>();

        /// <summary>
        /// Icons conteiner
        /// </summary>
        public ImageList imageList { get; set; }

        /// <summary>
        /// Search path
        /// </summary>
        public string searchPath { get; set; }

        private delegate void oneParameterDelegate(object obj);
        private oneParameterDelegate cur;
        private void setCurrentFile(object obj) {
            currentFile.Text = obj as string;
        }

        private oneParameterDelegate dub;
        private void setDublicate(object obj) {
            FileInfo file = obj as FileInfo;
            if (file == null)
                return;

            string[] items = new string[] { file.Name, file.Directory.FullName, string.Format("{0} КБ", file.Length / 1024), file.LastWriteTime.ToString(), NativeMethods.GetShellFileType(file.FullName) };
            switch (file.Extension) {
                case ".exe":
                case ".cur":
                case ".ico":
                case ".lnk":
                    imageList.Images.Add(file.FullName, Icon.ExtractAssociatedIcon(file.FullName));
                    dublicates.Items.Add(new ListViewItem(items, file.FullName));
                    break;
                default:
                    if (!imageList.Images.ContainsKey(file.Extension))
                        imageList.Images.Add(file.Extension, Icon.ExtractAssociatedIcon(file.FullName));

                    dublicates.Items.Add(new ListViewItem(items, file.Extension));
                    break;
            }
        }

        private delegate void withoutParametersDelegate();
        private withoutParametersDelegate pro;
        private void setProgress() {
            progress.Maximum = fileInfo.Count;
            progress.PerformStep();
            foundScan.Text = string.Format("Найдено одинаковых файлов {0}. Всего просканировано файлов {1}.", dublicatesCount, filesCount);
        }

        private static void GetFileNamesInDir(DirectoryInfo dir, List<FileInfo> arr) {
            foreach (DirectoryInfo curDir in dir.GetDirectories()) {
                try {
                    GetFileNamesInDir(curDir, arr);
                }
                catch (UnauthorizedAccessException c) { }
                catch (Exception e) {
                    MessageBox.Show(e.Message);
                }
            }
            arr.AddRange(dir.GetFiles());
        }

        public void ScanDirectory() {
            DirectoryInfo dir = new DirectoryInfo(searchPath);
            GetFileNamesInDir(dir, fileInfo);
            filesCount = fileInfo.Count;
            dublicatesCount = 0;
            cur = new oneParameterDelegate(setCurrentFile);
            dub = new oneParameterDelegate(setDublicate);
            pro = new withoutParametersDelegate(setProgress);
            for (int i = 0; i < fileInfo.Count; i++) {
                // CurrentFile.Text = new FileInfo(files[i]).Name;
                currentFile.Invoke(cur, fileInfo[i].Name);
                bool AlsoTime = false;
                for (int j = i + 1; j < fileInfo.Count; j++) {
                    if (fileInfo[i].Name == fileInfo[j].Name) {
                        if ((fileInfo[i].Length == fileInfo[j].Length) && (fileInfo[i].LastWriteTime == fileInfo[j].LastWriteTime)) {
                            if (AlsoTime == false) {
                                // DuplicateFile.Items.Add(new ListViewItem(files[i]));
                                dublicates.Invoke(dub, fileInfo[i]);
                                dublicatesCount++;
                                AlsoTime = true;
                            }
                            // DuplicateFile.Items.Add(new ListViewItem(files[j]));
                            dublicates.Invoke(dub, fileInfo[j]);
                            dublicatesCount++;
                            fileInfo.RemoveAt(j);
                        }
                    }
                }
                // progressbar does step
                progress.Invoke(pro);
            }
        }

    }
}
