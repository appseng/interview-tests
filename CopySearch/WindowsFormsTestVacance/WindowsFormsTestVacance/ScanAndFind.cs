﻿namespace WindowsFormsTestVacance {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using System.Windows.Forms;
    using System.Drawing;

    public partial class ScanAndFind : Form {
        public ScanAndFind() {
            InitializeComponent();
        }

        /// <summary>
        /// scanning was
        /// </summary>
        private bool scanning = false;

        /// <summary>
        /// Scanning thread
        /// </summary>
        private Thread thread = null;

        /// <summary>
        /// Start-button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Start_Click(object sender, EventArgs e) {
            if (scanning)
                return;

            scanning = true;
            Work w = new Work();
            w.currentFile = this.CurrentFile;
            w.progress = this.Progress;
            w.dublicates = this.DuplicateFile;
            w.dublicates.LargeImageList = w.dublicates.SmallImageList = this.imageList;
            w.foundScan = this.FoundScan;
            w.imageList = this.imageList;
            #if DEBUG
            w.searchPath = @"C:\Docu";
            //w.searchPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            #else
            DirectoryInfo di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.Windows));
            w.searchPath = di.Root.FullName;
            #endif

            this.thread = new Thread(new ThreadStart(w.ScanDirectory));
            this.thread.Start();
            this.CurrentFile.Text = string.Format("Началась обработка {0}", w.searchPath);

        }
        public ImageList imageList = new ImageList();

        /// <summary>
        /// Pause-button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Pause_Click(object sender, EventArgs e) {
            if (thread == null)
                return;

            if ((thread.ThreadState == ThreadState.Running) || (thread.ThreadState == ThreadState.WaitSleepJoin))
                thread.Suspend();
            else if (thread.ThreadState == ThreadState.Suspended)
                thread.Resume();
        }

        /// <summary>
        /// Stop-button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Stop_Click(object sender, EventArgs e) {
            if (thread == null)
                return;

            if ((thread.ThreadState == ThreadState.Running) || (thread.ThreadState == ThreadState.WaitSleepJoin))
                thread.Abort();
        }

        /// <summary>
        /// Closing Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            if (thread == null)
                return;

            if (thread.ThreadState == ThreadState.Suspended)
                thread.Resume();
            thread.Abort();
        }

    }
}