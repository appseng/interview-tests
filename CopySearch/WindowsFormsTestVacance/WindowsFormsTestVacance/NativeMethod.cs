﻿namespace WindowsFormsTestVacance
{
    using System;
    using System.Runtime.InteropServices;

    class NativeMethods
    {
        private const uint FILE_ATTRIBUTE_NORMAL = 0x80;
        private const uint SHGFI_TYPENAME = 0x400;
        private const uint SHGFI_USEFILEATTRIBUTES = 0x000000010;

        [DllImport("shell32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr SHGetFileInfo(
            string pszPath,
            uint dwFileAttributes,
            ref  SHFILEINFO shinfo,
            uint cbfileInfo,
            uint uFlags);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct SHFILEINFO
        {
            public IntPtr hIcon;
            public int iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };


        public static string GetShellFileType(string fileName)
        {
            var shinfo = new SHFILEINFO();
            const uint flags = SHGFI_TYPENAME | SHGFI_USEFILEATTRIBUTES;

            if (SHGetFileInfo(fileName, FILE_ATTRIBUTE_NORMAL, ref shinfo, (uint)Marshal.SizeOf(shinfo), flags) == IntPtr.Zero)
            {
                return "File";
            }
            return shinfo.szTypeName;
        }
    }
}
