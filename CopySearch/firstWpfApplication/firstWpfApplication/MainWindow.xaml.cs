﻿namespace firstWpfApplication {
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Threading;
    using System.IO;

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        /// <summary>
        /// scanning was
        /// </summary>
        private bool scanning = false;

        /// <summary>
        /// Scanning thread
        /// </summary>
        private Thread thread = null;

        /// <summary>
        /// Start-button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, RoutedEventArgs e) {
            if (scanning)
                return;

            scanning = true;
            Work w = new Work();
            w.CurrentFile = this.CurrentFile;
            w.Progress = this.Progress;
            w.Dublicates = this.DuplicateFile;
            w.Dublicates.ItemsSource = w.ObjectList;
            w.FoundScan = this.FoundScan;
            #if DEBUG
            //w.searchPath = @"F:\";
            w.SearchPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            #else
            DirectoryInfo di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.Windows));
            w.searchPath = di.Root.FullName;
            #endif

            this.thread = new Thread(new ThreadStart(w.ScanDirectory));
            this.thread.Start();
            this.CurrentFile.Content = string.Format("Началась обработка {0}", w.SearchPath);

        }

        /// <summary>
        /// Pause-button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, RoutedEventArgs e) {
            if (thread == null)
                return;

            if ((thread.ThreadState == ThreadState.Running) || (thread.ThreadState == ThreadState.WaitSleepJoin))
                thread.Suspend();
            else if (thread.ThreadState == ThreadState.Suspended)
                thread.Resume();
        }

        /// <summary>
        /// Stop-button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, RoutedEventArgs e) {
            if (thread == null)
                return;

            if ((thread.ThreadState == ThreadState.Running) || (thread.ThreadState == ThreadState.WaitSleepJoin))
                thread.Abort();
        }

        /// <summary>
        /// Closing Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if (thread == null)
                return;

            if (thread.ThreadState == ThreadState.Suspended)
                thread.Resume();
            thread.Abort();
        }
    }
}
