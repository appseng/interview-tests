﻿namespace firstWpfApplication {
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interop;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    public class Work {
        /// <summary>
        /// Label-control of processed filename
        /// </summary>
        public Label CurrentFile { get; set; }

        /// <summary>
        /// Control of progress-line
        /// </summary>
        public ProgressBar Progress { get; set; }

        /// <summary>
        /// Control of files list
        /// </summary>
        public ListView Dublicates { get; set; }

        /// <summary>
        /// Scanned and found files
        /// </summary>
        public Label FoundScan { get; set; }

        /// <summary>
        /// Count of dublicated files
        /// </summary>
        private int dublicatesCount;

        /// <summary>
        /// Count of all files
        /// </summary>
        private int filesCount;

        /// <summary>
        /// Informational list
        /// </summary>
        private List<FileInfo> fileInfo = new List<FileInfo>();

        /// <summary>
        /// Icons conteiner
        /// </summary>
        public ObservableCollection<ListViewFileItem> ObjectList = new ObservableCollection<ListViewFileItem>();
        /// <summary>
        /// Search path
        /// </summary>
        public string SearchPath { get; set; }

        private Dispatcher dis = Dispatcher.CurrentDispatcher;
        
        private delegate void oneParameterDelegate(object obj);
        private oneParameterDelegate cur;
        private void setCurrentFile(object obj) {
            CurrentFile.Content = obj as string;
        }

        private oneParameterDelegate dub;
        private void setDublicate(object obj) {
            FileInfo file = obj as FileInfo;
            if (file == null)
                return;

            ListViewFileItem item = new ListViewFileItem() {
                Icon = Imaging.CreateBitmapSourceFromHIcon(
                      Icon.ExtractAssociatedIcon(file.FullName).Handle,
                      Int32Rect.Empty,
                      BitmapSizeOptions.FromWidthAndHeight(16, 16)),
                Name = file.Name,
                Folder = file.Directory.FullName,
                Length = string.Format("{0} КБ", file.Length / 1024),
                LastWriteTime = file.LastWriteTime.ToString(),
                FileType = NativeMethods.GetShellFileType(file.FullName)
            };
            ObjectList.Add(item);
        }

        private delegate void withoutParametersDelegate();
        private withoutParametersDelegate pro;
        private void setProgress() {
            Progress.Maximum = fileInfo.Count;
            Progress.Value += 1;
            FoundScan.Content = string.Format("Найдено одинаковых файлов {0}. Всего просканировано файлов {1}.", dublicatesCount, filesCount);
        }

        public List<FileInfo> GetAllFileNames(string dirPath) {
            List<FileInfo> arr = new List<FileInfo>();
            DirectoryInfo dir = new DirectoryInfo(dirPath);
            GetFileNamesInDir(dir, arr);
            return arr;
        }

        private void GetFileNamesInDir(DirectoryInfo dir, List<FileInfo> arr) {
            foreach (DirectoryInfo curDir in dir.GetDirectories()) {
                try {
                    GetFileNamesInDir(curDir, arr);
                }
                catch (UnauthorizedAccessException c) { }
                catch (Exception e) {
                    MessageBox.Show(e.Message);
                }
            }
            arr.AddRange(dir.GetFiles("."));
        }

        public void ScanDirectory() {
            fileInfo = GetAllFileNames(SearchPath);
            filesCount = fileInfo.Count;
            dublicatesCount = 0;
            cur = new oneParameterDelegate(setCurrentFile);
            dub = new oneParameterDelegate(setDublicate);
            pro = new withoutParametersDelegate(setProgress);
            for (int i = 0; i < fileInfo.Count; i++) {
                // CurrentFile.Text = new FileInfo(files[i]).Name;
                dis.Invoke(cur, fileInfo[i].Name);
                //currentFile.Invoke(cur, fileInfo[i].Name);
                bool AlsoTime = false;
                for (int j = i + 1; j < fileInfo.Count; j++) {
                    if (fileInfo[i].Name == fileInfo[j].Name) {
                        if ((fileInfo[i].Length == fileInfo[j].Length) && (fileInfo[i].LastWriteTime == fileInfo[j].LastWriteTime)) {
                            if (AlsoTime == false) {
                                // DuplicateFile.Items.Add(new ListViewItem(files[i]));
                                //dublicates.Invoke(dub, fileInfo[i]);
                                dis.Invoke(dub, fileInfo[i]);
                                dublicatesCount++;
                                AlsoTime = true;
                            }
                            // DuplicateFile.Items.Add(new ListViewItem(files[j]));
                            //dublicates.Invoke(dub, fileInfo[j]);
                            dis.Invoke(dub, fileInfo[j]);
                            dublicatesCount++;
                            fileInfo.RemoveAt(j);
                        }
                    }
                }
                // progressbar does step
                //progress.Invoke(pro);
                dis.Invoke(pro, null);
            }
        }

    }
}
