﻿namespace firstWpfApplication {
    using System.Windows.Media;

    public class ListViewFileItem {
        public ImageSource Icon { get; set; }
        public string Name { get; set; }
        public string Folder { get; set; }
        public string Length { get; set; }
        public string LastWriteTime { get; set; }
        public string FileType { get; set; }
    }
}
